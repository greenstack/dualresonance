﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DualResonance.Abilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DualResonance.Deployables.Units;
using DualResonance.Deployables.Cards;

namespace DualResonance.Abilities.Tests
{
    [TestClass()]
    public class AbilityListTests
    {
        private static MercenaryCard testMerc = new MercenaryCard(10, 5, 5, 1, 0);

        private ActiveMercenary tmA1 = new ActiveMercenary(testMerc, Teams.Alpha);
        private ActiveMercenary tmA2 = new ActiveMercenary(testMerc, Teams.Alpha);
        private ActiveMercenary tmB1 = new ActiveMercenary(testMerc, Teams.Beta);
        private ActiveMercenary tmB2 = new ActiveMercenary(testMerc, Teams.Beta);

        [TestMethod()]
        public void SameTeamTest()
        {
            bool sameTeam = AbilityList.TeamCheck(tmA1, tmA2, true);
            Assert.AreEqual(true, sameTeam);
        }

        [TestMethod()]
        public void DifferentTeamTest()
        {
            bool differentTeam = AbilityList.TeamCheck(tmA1, tmB1);
            Assert.AreEqual(true, differentTeam);
        }

        [TestMethod()]
        public void ExtraMileTest()
        {
            MercenaryCard extraMileMerc = new MercenaryCard(10, 5, 5, 0, 0)
            {
                A = AbilityList.ExtraMile,
                B = AbilityList.None,
            };

            ActiveMercenary beeGirl = new ActiveMercenary(MercenaryCards.GetMercenary("Bee Girl"), Teams.Alpha);
            ActiveMercenary extraMiler = new ActiveMercenary(extraMileMerc, Teams.Alpha);

            beeGirl.UseAbilityA(extraMiler);

            Assert.AreEqual(7, extraMiler.AttackPower);
            Assert.AreEqual(7, extraMiler.WillPower);
        }

        [TestMethod()]
        public void PricklyArmorTest()
        {
            MercenaryCard pricklyBoi = new MercenaryCard(10, 5, 5, 5, 0)
            {
                A = AbilityList.PricklyArmor,
                B = AbilityList.None,
            };

            ActiveMercenary pricklyBoiU = new ActiveMercenary(pricklyBoi, Teams.Alpha);

            tmB1.Attack(pricklyBoiU);
            Assert.AreEqual(9, tmB1.HealthPoints);
            tmB1.Tick();
        }

        [TestMethod()]
        public void SharedKnowledgeTest()
        {
            MercenaryCard knowyBoi = new MercenaryCard(10, 5, 5, 5, 0)
            {
                A = AbilityList.SharedKnowledge,
                B = AbilityList.None,
            };


            ActiveMercenary beeGirl = new ActiveMercenary(MercenaryCards.GetMercenary("Bee Girl"), Teams.Alpha);
            ActiveMercenary shareyBoi = new ActiveMercenary(knowyBoi, Teams.Alpha);
            beeGirl.UseAbilityA(shareyBoi);
            Assert.AreEqual(5, beeGirl.WillPower);
        }
    }
}