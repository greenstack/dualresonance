﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DualResonance.Status
{
    /// <summary>
    /// Manages how statuses should be applied. Since only one status of each
    /// type (Boon, Ailment) can be applied to a mercenary at a time, this enum
    /// is used to determine which one should take precedence. Any status with
    /// equivalent precedence will overwrite the other. This means that any
    /// status can't be consistently renewed (though perhaps powered up).
    /// </summary>
    public enum StatusPrecedence
    {
        /// <summary>
        /// Any higher-priority status will overwrite this one.
        /// </summary>
        Lowest,
        /// <summary>
        /// Any status with higher priority will override this precedence.
        /// </summary>
        Low, 
        /// <summary>
        /// This status can be overridden by many others.
        /// </summary>
        Medium,
        /// <summary>
        /// Almost no other status will override this one.
        /// </summary>
        High,
        /// <summary>
        /// No other statuses can override this one.
        /// </summary>
        Maximum

    }
}
