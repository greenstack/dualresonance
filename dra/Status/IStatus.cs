﻿using DualResonance.Deployables.Units;

namespace DualResonance.Status
{
    public interface IStatus : ITick
    {
        int Severity { get; }
        int Duration { get; }
        StatusPrecedence Precedence { get; }
        void Apply(ActiveMercenary mercenary);
    }

    // TODO: turn these into classes that inherit from StatusBase.
    public interface IAilmentStatus : IStatus
    {
    }

    public interface IBoonStatus : IStatus
    {
        
    }
}
