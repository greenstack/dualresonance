﻿using DualResonance.Deployables.Units;

namespace DualResonance.Status
{
    /// <summary>
    /// The disabled class is interesting in that it isn't counted as neither an Ailment nor a Boon.
    /// </summary>
    public class DisabledStatus : StatusBase
    {
        /// <summary>
        /// Creates a DisabledStatus object.
        /// </summary>
        /// <param name="duration">The duration of the disabling. Cannot be infinite (a negative number).</param>
        public DisabledStatus(int duration) : base(duration, 0, StatusPrecedence.High)
        {
            if (duration < 0) throw new System.ArgumentOutOfRangeException("Disabled status cannot have negative (infinite) durations.");
        }

        /// <summary>
        /// Causes the mercenary from being unable to act.
        /// </summary>
        /// <param name="mercenary">The mercenary affected by the status.</param>
        public override void Apply(ActiveMercenary mercenary)
        {
            // There's no such thing as permanent disabling.
            // Remove the mercenary's disabledness by making it null when the
            // internal counter reaches zero.
            if (Duration <= 0)
                mercenary.disabled = null;
        }

        public override string ToString()
        {
            return $"Disabled ({Duration} turns remaining)";
        }
    }
}
