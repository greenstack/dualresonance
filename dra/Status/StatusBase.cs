﻿using DualResonance.Deployables.Units;

namespace DualResonance.Status
{
    /// <summary>
    /// Base class for any Status class. Any class that inherits from this should also
    /// implement the IBoonStatus or IAilmentStatus as well.
    /// </summary>
    public abstract class StatusBase : IStatus
    {
        public StatusPrecedence Precedence
        {
            get;
            protected set;
        }

        public int Severity { get; protected set; }

        public int Duration { get; protected set; }
        
        protected StatusBase(int duration, int severity, StatusPrecedence precedence)
        {
            Duration = duration;
            Severity = severity;
            Precedence = precedence;
        }

        public abstract void Apply(ActiveMercenary mercenary);

        public virtual void Tick()
        {
            Duration--;
        }
        /// <summary>
        /// Helps determine which status is more powerful (and therefore, overriding).
        /// </summary>
        /// <param name="first">The first object being compared to.</param>
        /// <param name="second">The second object.</param>
        /// <returns>True if the precedence of the first status is greater than the precedence of the second.</returns>
        public static bool operator>(StatusBase first, StatusBase second)
        {
            return first.Precedence > second.Precedence;
        }

        /// <summary>
        /// Helps determine which status is more powerful (and therefore, overriding).
        /// </summary>
        /// <param name="first">The first object being compared to.</param>
        /// <param name="second">The second object.</param>
        /// <returns>True if the precedence of the first status is less than the precedence of the second.</returns>
        public static bool operator<(StatusBase first, StatusBase second)
        {
            return first.Precedence < second.Precedence;
        }

        public abstract override string ToString();
    }
}
