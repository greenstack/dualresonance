﻿namespace DualResonance
{
    public interface IPlayer
    {
        Teams Team { get; set; }
        int PlayerId { get; set; }
        DeckInfo CurrentDeck { get; set; }
    }
}
