﻿using System.Collections.Generic;
using System.Linq;
using DualResonance.Commands;
using DualResonance.Deployables.Units;

namespace DualResonance
{
    /// <summary>
    /// Represents a Player that is controlled by an AI.
    /// </summary>
    public class AIPlayer : Player
    {
        System.Random rand = new System.Random();

        public Queue<AbstractCommand> BuildTurn(GameSession session)
        {
            Queue<AbstractCommand> turn = new Queue<AbstractCommand>();

            SpawnUnits(session, turn);

            CommandUnits(session, turn);

            turn.Enqueue(new EndTurnCommand(this));

            return turn;
        }

        private void SpawnUnits(GameSession session, Queue<AbstractCommand> turn)
        {
            if (CurrentDeck == null || CurrentDeck.MercenaryIds.Count == 0) return;
            ActiveCommander aicmdr = session.GetPlayerCommander(PlayerId);
            BoardCoordinate spawnAt;
            int maxTries = 10;
            int tries = 0;
            do
            {
                spawnAt = new BoardCoordinate(rand.Next(0, 5), rand.Next(0, 2));
            } while (session.Query(spawnAt) != null && tries++ < maxTries);
            int mercId, deckpos;
            tries = 0;
            maxTries = CurrentDeck.UnitsInDeck;
            do
            {
                deckpos = rand.Next(0, CurrentDeck.UnitsInDeck);
                mercId = CurrentDeck.GetMercenary(deckpos).Id;
                // TODO: figure out gold cost for ai? or just give him the money to pay for it
            } while (session.Board.GetMercsWithId(mercId).Count() == 1 && tries++ < maxTries);
            aicmdr.CurrentGold = Deployables.Cards.MercenaryCards.GetMercenary(mercId).GoldCost;
            SpawnCommand spawnCommand = new SpawnCommand(this, spawnAt, deckpos);
            turn.Enqueue(spawnCommand);
        }

        private void CommandUnits(GameSession session, Queue<AbstractCommand> turn)
        {
            IEnumerable<ActiveMercenary> mercenaries = session.Board.GetPlayersActiveMercs(this);
            foreach (var ally in mercenaries)
            {
                if (!ally.CanAct) continue;
                var opponents = session.Board.GetMercsInCardinalDirections(ally.Location, this, false);
                AbstractCommand command = null;
                foreach (var foe in opponents)
                {
                    if (ally.NextTo(foe))
                    {
                        command = new InvokeCommand(
                            this,
                            ally.Location,
                            ally.DirectionTo(foe),
                            AbilityId.Attack);
                    }
                    else continue;

                    turn.Enqueue(command);
                    goto NextAlly;
                }

                // Unit didn't find
                if (command == null)
                {
                    command = new MoveCommand(
                            this,
                            ally.Location,
                            Direction.South);
                    turn.Enqueue(command);
                    command = null;
                    var newLoc = ally.Location + BoardCoordinate.FromDirection(Direction.South);
                    // Check for enemies nearby
                    foreach (var foe2 in opponents)
                    {
                        if (newLoc.DistanceTo(foe2.Location) == 1)
                        {
                            command = new InvokeCommand(
                                this, newLoc,
                                newLoc.DirectionTo(foe2.Location),
                                AbilityId.Attack);
                            goto ProcessAttack;
                        }
                        else command = null;
                    }
                    ProcessAttack:
                    // Have the unit rest
                    newLoc = session.Board.CoordinateIsInBounds(newLoc) ? newLoc : ally.Location;
                    if (command == null) // Attack the commander
                        command = new InvokeCommand(this, newLoc, Direction.South, AbilityId.Attack);
                    turn.Enqueue(command);
                }

                NextAlly: continue;
            }
        }
    }
}
