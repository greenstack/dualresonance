﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DualResonance.Deployables
{
    public enum RemovalCause
    {
        /// <summary>
        /// For cards that are removed due to timeouts.
        /// </summary>
        Expired,
        /// <summary>
        /// For cards that are destroyed or killed.
        /// </summary>
        Killed,
        /// <summary>
        /// For cards that are voluntarily withdrawn.
        /// </summary>
        Withdrawn,
        /// <summary>
        /// For cards with a WillPower stat.
        /// </summary>
        Lunacy
    }
}
