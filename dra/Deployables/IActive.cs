﻿namespace DualResonance.Deployables
{
    public interface IActive : ITick, Abilities.IAbilityUser
    {
        /// <summary>
        /// The current state of the unit.
        /// </summary>
        Units.IUnitState UnitState { get; set; }
        int HealthPoints { get; }
        Teams Team { get; }
        bool CanAct { get; }

        /// <summary>
        /// Called when the character dies or is otherwise removed from play.
        /// </summary>
        void RemoveFromPlay(RemovalCause cause);
        void DecrementHealthPoints(int amount);

        void Rest();
    }
}
