﻿using DualResonance.Abilities;

namespace DualResonance
{
    public interface ICharacter : IAbled, INamed
    {
        int MaxHP { get; }
    }
}
