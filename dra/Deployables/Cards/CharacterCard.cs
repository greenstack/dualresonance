﻿using DualResonance.Abilities;

namespace DualResonance
{
    public abstract class CharacterCard : ICharacterCard, ICostGold
    {
        /// <summary>
        /// The name of the Character.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// A text describing the character.
        /// </summary>
        public string FlavorText { get; set; }

        /// <summary>
        /// The character's A Ability. Ability A
        /// </summary>
        public Ability A { get; set; } = AbilityList.None;
        /// <summary>
        /// The character's B Ability.
        /// </summary>
        public Ability B { get; set; } = AbilityList.None;
        /// <summary>
        /// The cost required to cast this character.
        /// </summary>
        public abstract int GoldCost { get; }
    }
}
