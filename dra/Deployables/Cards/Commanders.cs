﻿using System;

namespace DualResonance.Deployables.Cards
{
    public static class Commanders
    {
        public const int CopperId = 0;
        public static readonly CommanderCard Copper = new CommanderCard()
        {
            Name = "Copper Bitner",
            Epithet = "Exiled Knight",
            FlavorText = "A Defense Mercenaries Inc. member from Midway Valley. Seeks her father.",
            GoldPerTurn = 4,
            ResonancePerTurn = 4,
            PrimaryResonance = ResonanceType.Barrier,
            SecondaryResonance = ResonanceType.Burst,
        };

        public const int RunnerId = 1;
        public static readonly CommanderCard Runner = new CommanderCard()
        {
            Name = "Runner",
            Epithet = "Reformed Outlaw",
            FlavorText = "An aeran smuggler with a salacious past who is changing his ways.",
            GoldPerTurn = 5,
            ResonancePerTurn = 3,
            PrimaryResonance = ResonanceType.Psyche,
            SecondaryResonance = ResonanceType.Triage,
        };

        public const int TaminId = 2;
        public static readonly CommanderCard Tamin = new CommanderCard()
        {
            Name = "Tamin",
            Epithet = "Lost Schirip",
            FlavorText = "A being not of this world. She was recently freed from Raider captivity.",
            GoldPerTurn = 2,
            ResonancePerTurn = 6,
            PrimaryResonance = ResonanceType.Burst,
            SecondaryResonance = ResonanceType.Distort,
        };

        public const int AlmanaughtId = 3;
        public static readonly CommanderCard Almanaught = new CommanderCard()
        {
            Name = "Almanaught",
            Epithet = "Horror Incarnate",
            FlavorText = "The apparent leader of the Raiders. Nobody can really decipher his motives.",
            GoldPerTurn = 3,
            ResonancePerTurn = 5,
            PrimaryResonance = ResonanceType.Distort,
            SecondaryResonance = ResonanceType.Psyche,
        };

        public const int FerianId = 4;
        public static readonly CommanderCard Ferian = new CommanderCard()
        {
            Name = "Princess Ferian",
            Epithet = "Cunning Heiress",
            FlavorText = "The heir to the Resonant Throne. Her lust for power knows no bounds.",
            GoldPerTurn = 4,
            ResonancePerTurn = 4,
            PrimaryResonance = ResonanceType.Triage,
            SecondaryResonance = ResonanceType.Barrier,
        };

        public static CommanderCard GetCard(int id)
        {
            switch (id)
            {
                case CopperId: return Copper;
                case RunnerId: return Runner;
                case TaminId: return Tamin;
                case AlmanaughtId: return Almanaught;
                case FerianId: return Ferian;
                default: throw new ArgumentOutOfRangeException("Invalid index was supplied.");
            }
        }
    }
}
