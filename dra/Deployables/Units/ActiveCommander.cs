﻿using System;
using DualResonance.Abilities;
using DualResonance.Deployables.Cards;

namespace DualResonance.Deployables.Units
{
    public class ActiveCommander : ActiveBase, ICommander
    {
        private readonly CommanderCard commander;

        public const int MAX_GOLD = 15;

        public int GoldPerTurn { get => commander.GoldPerTurn; }
        public int ResonancePerTurn { get => commander.ResonancePerTurn; }
        // I need to change how this works. It's not dynamic enough.
        public ResonanceType PrimaryResonance { get; set; }
        public ResonanceType SecondaryResonance { get; set; }
        public ResonanceType GeneratedResonances
            { get => PrimaryResonance | SecondaryResonance; }

        public int CurrentResonance = 0;
        private int currentGold = 0;
        public int CurrentGold
        {
            get => currentGold;
            set => currentGold = Math.Min(value, MAX_GOLD);
        }

        public int MaxHP => commander.MaxHP;

        public override string Name => commander.Name;

        public override string FlavorText => commander.FlavorText;

        public string Epithet => commander.Epithet;

        public override Ability A => commander.A;

        public override Ability B => commander.B;

        public override bool CanAct => throw new NotImplementedException();

        public ActiveCommander(CommanderCard commander, Teams team) : base(team)
        {
            this.commander = commander;
            HealthPoints = MaxHP;
            PrimaryResonance = commander.PrimaryResonance;
            SecondaryResonance = commander.SecondaryResonance;
        }

        public override void Tick()
        {
            CurrentGold += GoldPerTurn;
            CurrentResonance += ResonancePerTurn;
        }

        internal override void ReceiveAbilityAttack(IActive caster, Ability ability)
        {
            //throw new NotImplementedException();
        }

        internal override void ReceiveAttack(ActiveBase attacker)
        {
            //throw new NotImplementedException();
        }
    }
}
