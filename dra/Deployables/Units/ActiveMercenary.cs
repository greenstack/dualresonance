﻿using DualResonance.Abilities;
using DualResonance.Deployables.Cards;
using DualResonance.Status;
using System;
using System.Collections.Generic;

namespace DualResonance.Deployables.Units
{
    // TODO: Decide if RP is something I still want to have.
    // TODO: Implement Ailments. (later, if there's time)
    // TODO: Implement Boons. (later, if there's time)
    public class ActiveMercenary : AbstractMoveableUnit, IMercenary
    {
        private MercenaryCard mercenary;

        public int OwnerPlayerId = -1;

        public override string Name { get => mercenary.Name; }
        public override string FlavorText { get => mercenary.FlavorText; }

        public int MercenaryId
        {
            get => mercenary.Id;
        }

        private int attackPower;
        /// <summary>
        /// The merc's current attack power.
        /// </summary>
        public int AttackPower => attackPower + totalDifference.APMod;

        internal Ability attack = AbilityList.Attack;

        private int willPower;
        /// <summary>
        /// The merc's current willpower.
        /// </summary>
        public int WillPower => willPower + totalDifference.WPMod;

        StatModifier totalDifference = new StatModifier(0,0,-1);

        List<StatModifier> statChanges = new List<StatModifier>();

        public delegate void OnReact(ActiveMercenary sender, ActivationArgs args);
        /// <summary>
        /// Triggers when the unit is attacked.
        /// </summary>
        private event OnReact Retaliate = (sender, args) => { };
        /// <summary>
        /// Triggers when the unit is destroyed.
        /// </summary>
        private event OnReact Vengeance = (sender, args) => { };
        /// <summary>
        /// Triggers when the unit's stats are changed.
        /// </summary>
        private event OnReact Concentrate = (sender, args) => { };
        /// <summary>
        /// Triggers when the unit is targeted by an ability.
        /// </summary>
        private event OnReact Feedback = (sender, args) => { };

        public int MaxHP => mercenary.MaxHP;
        public int MaxAP => mercenary.MaxAP;
        public int MaxWP => mercenary.MaxWP;

        public override Ability A { get => mercenary.A; }
        public override Ability B { get => mercenary.B; }

        public override bool CanAct {
            get
            {
                return disabled == null && !(UnitState is States.UnitRestingState);
            }
        }

        public IAilmentStatus Ailment { get; set; }
        public DisabledStatus disabled { get; set; }
        public IBoonStatus Boon { get; set; }

        public ActiveMercenary(MercenaryCard main, Teams team) : base(team)
        {
            mercenary = main;
            HealthPoints = main.MaxHP;
            attackPower = main.MaxAP;
            willPower = main.MaxWP;
            Remove += (sender, args) => { };
            // Register passive ability events
            ProcessAbility(main.A);
            ProcessAbility(main.B);
        }
        
        private void ProcessAbility(Ability ability)
        {
            if (ability is PassiveAbility passive)
                // Register this ability's activate effect to the right event
                switch (passive.Trigger)
                {
                    case AbilityTrigger.Command:
                        break;
                    case AbilityTrigger.PreUserAttack:
                        break;
                    case AbilityTrigger.PostUserAttack:
                        break;
                    case AbilityTrigger.PreFoeAttack:
                        break;
                    case AbilityTrigger.PostFoeAttack:
                        Retaliate += passive.Activate;
                        break;
                    case AbilityTrigger.Destroyed:
                        Vengeance += passive.Activate;
                        break;
                    case AbilityTrigger.StatModified:
                        Concentrate += passive.Activate;
                        break;
                    case AbilityTrigger.TargetedByAbility:
                        Feedback += passive.Activate;
                        break;
                    default:
                        break;
                }
        }

        /// <summary>
        /// Modifies the merc's stats for an amount of time. Because any Concentrate effects are implemented before the actual
        /// change is applied to the mercenary, Concentrate abilities can modify the stats before they are actually applied.
        /// </summary>
        /// <param name="apMod">The amount by which to modify the merc's AP.</param>
        /// <param name="wpMod">The amount by which to modify the merc's WP.</param>
        /// <param name="duration">How long the stat changes should take effect. A negative number represents infinity.</param>
        /// <param name="changer">The one causing the changes to the stats.</param>
        public void ModifyStats(int apMod, int wpMod, int duration, ActiveMercenary changer = null)
        {
            StatModifier newChange = new StatModifier(apMod, wpMod, duration);
            var conArgs = new ConcentrationArgs(changer, null, newChange);
            Concentrate(this, conArgs);
            newChange = conArgs.Change; // To handle changes from passive abilities on the changes.
            statChanges.Add(newChange);
            totalDifference += newChange;
        }

        /// <summary>
        /// This function should be run at the end of each round, though an ability may cause it to be run.
        /// </summary>
        public override void Tick()
        {
            // Check the various statuses.
            if (Boon != null) {
                Boon.Tick();
                Boon.Apply(this);
            }
            if (Ailment != null)
            {
                Ailment.Tick();
                Ailment.Apply(this);
            }
            if (disabled != null)
            {
                disabled.Tick();
                disabled.Apply(this);
            }

            // Update the changes to the stats.
            List<StatModifier> modifiers = new List<StatModifier>();
            foreach (var mod in statChanges)
            {
                mod.Tick();
                if (mod.Duration == 0)
                    totalDifference -= mod;
                else
                    modifiers.Add(mod);
            }
            statChanges = modifiers;
            UnitState.Refresh(this);
        }

        /// <summary>
        /// Clears the changes. If both positives and negatives are true, all are eliminated.
        /// </summary>
        /// <param name="positives">If positive stat changes should be removed.</param>
        /// <param name="negatives">If negative stat changes should be removed.</param>
        internal void ClearStatChanges(bool positives, bool negatives)
        {
            if (positives && negatives)
            {
                statChanges = new List<StatModifier>();
                totalDifference = new StatModifier(-1, 0, 0);
                return;
            }
            var newList = new List<StatModifier>();
            foreach (var sm in statChanges)
            {
                StatModifier mod;
                if (positives)
                {
                    // It's entirely positive, so we need to get rid of it.
                    if (sm.APMod > 0 && sm.WPMod > 0)
                    {
                        totalDifference -= sm;
                        continue;
                    }
                    else if (sm.APMod > 0)
                        mod = new StatModifier(sm.Duration, 0, sm.WPMod);
                    else if (sm.WPMod > 0)
                        mod = new StatModifier(sm.Duration, sm.APMod, 0);
                    else
                        mod = sm;
                }
                else if (negatives)
                {
                    // It's entirely negative, so we need to get rid of it.
                    if (sm.APMod < 0 && sm.WPMod < 0)
                    {
                        totalDifference -= sm;
                        continue;
                    }
                    else if (sm.APMod < 0)
                        mod = new StatModifier(sm.Duration, 0, sm.WPMod);
                    else if (sm.WPMod < 0)
                        mod = new StatModifier(sm.Duration, sm.APMod, 0);
                    else
                        mod = sm;
                }
                else break; // We shouldn't have reached this point
                totalDifference -= sm;
                totalDifference += mod;
                newList.Add(mod);
            }
        }

        public void Attack(ActiveBase target)
        {
            // Pre-attack abilities react here.
            if (UnitState.InvokeOnTarget(this, target, attack))
                target.ReceiveAttack(this);
        }

        public bool InvokeAbility(ActiveBase target, AbilityId abilityId)
        {
            // Pre-ability abilities react here.
            var ability = abilityId == AbilityId.A ? A : B;
            bool result = UnitState.InvokeOnTarget(this, target, ability);
            if (result)
                target.ReceiveAbilityAttack(this, ability);
            return result;
        }

        internal override void ReceiveAbilityAttack(IActive attacker, Ability ability)
        {
            // This is where the Feedback abilities will be activated.
            Feedback(this, new ActivationArgs((ActiveMercenary)attacker, null));
        }

        internal override void ReceiveAttack(ActiveBase attacker)
        {
            // This is where the Retaliate abilities will be activated.
            Retaliate(this, new ActivationArgs((ActiveMercenary)attacker, null));
        }
    }
}
