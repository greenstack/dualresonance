﻿using System;
using DualResonance.Abilities;
using DualResonance.Logger;

namespace DualResonance.Deployables.Units
{
    public class DeathEventArgs : EventArgs
    {
        /// <summary>
        /// The location of the unit on the board, if applicable.
        /// </summary>
        public BoardCoordinate location;
        public RemovalCause cause;
    }

    public abstract class ActiveBase : IActive, INamed
    {
        #region Fields and Properties
        public const int MAX_MOVEMENT = 2;

        private int healthPoints;
        /// <summary>
        /// The merc's current health.
        /// </summary>
        public int HealthPoints
        {
            get => healthPoints;
            protected set
            {
                healthPoints = value;
                if (healthPoints <= 0)
                {
                    healthPoints = 0;
                    RemoveFromPlay(RemovalCause.Killed);
                }
            }
        }

        public IUnitState UnitState { get; set; } = new States.SpryUnitState();

        public Teams Team { get; protected set; }

        /// <summary>
        /// If the unit is able to act.
        /// </summary>
        public abstract bool CanAct { get; }
        #endregion Fields and Properties

        protected ActiveBase(Teams team)
        {
            Team = team;
        }

        #region Methods
        /// <summary>
        /// Invokes the unit's A ability.
        /// </summary>
        /// <param name="user">The user of the ability.</param>
        /// <param name="target"></param>
        public void UseAbilityA(IActive target)
        {
            UseAbility((ActiveBase)target, A);
            
        }

        public void UseAbilityB(IActive target)
        {
            UseAbility((ActiveBase)target, B);
        }

        private void UseAbility(IActive target, Ability ability)
        {
            if (UnitState.InvokeOnTarget(this, target, ability) && target is ActiveBase tactive)
            {
                tactive.ReceiveAbilityAttack(this, A);
                Log.Instance.RegisterEvent(new AbilityUsedMessage(this, (ActiveBase)target, ability));
            }
        }

        /// <summary>
        /// Verifies that the target can be the recipient of the A ability.
        /// </summary>
        /// <param name="target">The active body on the receiving end of the ability.</param>
        /// <returns>True if the target is indeed valid.</returns>
        public bool VerifyValidTargetForA(IActive target)
        {
            return A.VerifyValidTarget(this, target);
        }

        /// <summary>
        /// Verifies that the target can be the recipient of the B ability.
        /// </summary>
        /// <param name="target">The active body on the receiving end of the ability.</param>
        /// <returns>True if the target is indeed valid.</returns>
        public bool VerifyValidTargetForB(IActive target)
        {
            return B.VerifyValidTarget(this, target);
        }

        /// <summary>
        /// Reduces this merc's HP by the specified amount. If it reaches zero, the unit is destroyed.
        /// </summary>
        /// <param name="amount"></param>
        public void DecrementHealthPoints(int amount)
        {
            Log.Instance.RegisterEvent(new HealthChangedMessage(amount, this));
            HealthPoints -= amount;
        }

        /// <summary>
        /// This function is called when HP or WP drop to zero or lower. It may be called in other places, too.
        /// </summary>
        /// <param name="cause"></param>
        public void RemoveFromPlay(RemovalCause cause)
        {
            Console.WriteLine("Removed");
            DeathEventArgs deathArgs = new DeathEventArgs()
            {
                location = this is ILocatable ? ((ILocatable)this).Location : BoardCoordinate.OffBoard,
                cause = cause,
            };
            Log.Instance.RegisterEvent(new UnitDestroyedMessage(this, cause));
            Remove(this, deathArgs);
        }

        public void Rest()
        {
            UnitState.Rest(this);
        }

        #endregion Methods

        public delegate void OnRemoval(object sender, DeathEventArgs args);
        public event OnRemoval Remove;

        #region Abstract fields and methods
        public abstract Ability A { get; }
        public abstract Ability B { get; }

        public abstract string Name { get; }
        public abstract string FlavorText { get; }

        /// <summary>
        /// Causes the active unit to receive an attack.
        /// </summary>
        /// <param name="attacker">The opposing mercenary launching the attack.</param>
        internal abstract void ReceiveAttack(ActiveBase attacker);

        /// <summary>
        /// Causes the active body to receive an ability.
        /// </summary>
        /// <param name="attacker">The unit attacking this unit.</param>
        /// <param name="ability">The ability used on this unit.</param>
        internal abstract void ReceiveAbilityAttack(IActive attacker, Ability ability);

        public abstract void Tick();
        #endregion Abstract fields and methods
    }
}
