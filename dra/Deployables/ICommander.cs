﻿namespace DualResonance.Deployables
{
    /// <summary>
    /// An interface for all commanders.
    /// </summary>
    public interface ICommander : ICharacter
    {
        /// <summary>
        /// The amount of gold that the commander gains per turn.
        /// </summary>
        int GoldPerTurn { get; }
        /// <summary>
        /// The amount of resonance the commander gains each turn.
        /// </summary>
        int ResonancePerTurn { get; }

        /// <summary>
        /// The commander's primary resonance type.
        /// </summary>
        ResonanceType PrimaryResonance { get; set; }
        /// <summary>
        /// The commander's secondary resonance type.
        /// </summary>
        ResonanceType SecondaryResonance { get; set; }
        /// <summary>
        /// The resonances that the commander generates together.
        /// 
        /// This should be equal to PrimaryResonance | SecondaryResonance.
        /// </summary>
        ResonanceType GeneratedResonances { get; }
        /// <summary>
        /// The commander's epithet.
        /// </summary>
        string Epithet { get; }
    }
}
