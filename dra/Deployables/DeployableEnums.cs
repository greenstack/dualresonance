﻿namespace DualResonance.Deployables
{
    public enum Race
    {
        Metalwrought,
        Schirip,
        Terran,
        Luxan,
        Floran,
        Aeran,
        Pyran,
        Unknown,
    }

    public enum Faction
    {
        Knight,
        Raider,
        Assassin,
        Civilian,
        Soldier,
        Pilot,
        Cultist
    }
}
