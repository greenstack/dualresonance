﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DualResonance
{
    [Flags]
    public enum Direction
    {
        // The first bit inverts the direction of the vertical component.
        // The second bit inverts the direction of the horizontal component.
        None = 0b0000,
        North = 0b0001,
        South = 0b1001,
        East = 0b0010,
        West = 0b0110,
    }
}
