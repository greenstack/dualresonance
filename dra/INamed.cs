﻿namespace DualResonance
{
    /// <summary>
    /// For objects that are named entities.
    /// </summary>
    public interface INamed
    {
        /// <summary>
        /// The name of this card.
        /// </summary>
        string Name { get; }
        /// <summary>
        /// The flavor text for this card.
        /// </summary>
        string FlavorText { get; }
    }
}
