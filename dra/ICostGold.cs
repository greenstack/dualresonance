﻿namespace DualResonance
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICostGold
    {
        /// <summary>
        /// The cost of this item in gold.
        /// </summary>
        int GoldCost { get; }
    }
}
