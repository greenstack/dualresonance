﻿namespace DualResonance
{
    /// <summary>
    /// Represents an object that requires resonance to create or cast.
    /// </summary>
    public interface ICostResonance
    {
        int BarrierRequirement { get; }
        int BurstRequirement { get; }
        int PsycheRequirement { get; }
        int DistortRequirement { get; }
        int TriageRequirement { get; }
        /// <summary>
        /// Retrieves the total amount of resonance required for this object.
        /// </summary>
        /// <param name="resonances">The resonances that are generated.</param>
        /// <returns>The amount of resonance required, according to the generated resonance.</returns>
        int GetTotalCost(ResonanceType resonances);
    }
}
