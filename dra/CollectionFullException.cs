﻿using System;

namespace DualResonance
{
    class CollectionFullException : Exception
    {
        public CollectionFullException(string Message) : base(Message)
        {
        }
    }
}
