﻿using System;

namespace DualResonance.Utilities
{
    public static class Comparisons
    {
        /*public static bool Equals(int left, int right) => left == right;

        public static bool GreaterThan(int left, int right) => left > right;

        public static bool GreaterThanEqualTo(int left, int right) => left >= right;

        public static bool LessThan(int left, int right) => left < right;

        public static bool LessThanEqualTo(int left, int right) => left <= right;
        */
        public static Func<int, int, bool> FromComparer(Comparer c)
        {
            switch (c)
            {
                case Comparer.EqualTo:
                    return new Func<int, int, bool>((left, right) => left == right);
                case Comparer.GreaterThan:
                    return new Func<int, int, bool>((left, right) => left > right);
                case Comparer.GreaterThanEqualTo:
                    return new Func<int, int, bool>((left, right) => left >= right);
                case Comparer.LessThan:
                    return new Func<int, int, bool>((left, right) => left < right);
                default:
                    return new Func<int, int, bool>((left, right) => left <= right);
            }
        }

        public static Func<int, int, bool> FromString(string str)
        {
            switch (str)
            {
                case "<":
                    return new Func<int, int, bool>((left, right) => left < right);
                case "<=":
                    return new Func<int, int, bool>((left, right) => left <= right);
                case "=":
                    return new Func<int, int, bool>((left, right) => left == right);
                case ">=":
                    return new Func<int, int, bool>((left, right) => left >= right);
                case ">":
                    return new Func<int, int, bool>((left, right) => left > right);
                default:
                    throw new ArgumentException("Invalid: " + str);
            }
        }

        public static Comparer CFromString(string str)
        {
            switch (str)
            {
                case "<":
                    return Comparer.LessThan;
                case "<=":
                    return Comparer.LessThanEqualTo;
                case "=":
                    return Comparer.EqualTo;
                case ">=":
                    return Comparer.GreaterThanEqualTo;
                case ">":
                    return Comparer.GreaterThan;
                default:
                    throw new ArgumentException("Invalid: " + str);
            }
        }
    }

    public enum Comparer
    {
        GreaterThan,
        GreaterThanEqualTo,
        LessThan,
        LessThanEqualTo,
        EqualTo
    }
}
