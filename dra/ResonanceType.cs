﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DualResonance
{
    /// <summary>
    /// These represent the resonances that a commander can generate.
    /// </summary>
    [Flags]
    public enum ResonanceType
    {
        None = 0b00000,
        Barrier = 0b00001,
        Burst =   0b00010,
        Psyche =  0b00100,
        Distort = 0b01000,
        Triage =  0b10000
    }
}
