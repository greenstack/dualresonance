﻿namespace DualResonance.Abilities
{
    public enum AbilityTrigger
    {
        /// <summary>
        /// This ability is only triggered by direct command.
        /// </summary>
        Command,
        /// <summary>
        /// This ability may be triggered when the user is about to attack.
        /// </summary>
        PreUserAttack,
        /// <summary>
        /// This ability may be triggered when the user has finished an attack. 
        /// </summary>
        PostUserAttack,
        /// <summary>
        /// This ability may be triggered when the unit is about to be attacked. 
        /// </summary>
        PreFoeAttack,
        /// <summary>
        /// This ability may be triggered after the unit has been attacked. Tied to the "Retaliate" keyword.
        /// </summary>
        PostFoeAttack,
        /// <summary>
        /// This ability may be triggered when the unit is destroyed. Tied to the "Vengeance" keyword.
        /// </summary>
        Destroyed,
        /// <summary>
        /// This ability is triggered when the unit's stats are modified. Tied to the "Concentrate" keyword.
        /// </summary>
        StatModified,
        /// <summary>
        /// This ability is triggered when the unit is targeted by an ability. Tied to the "Feedback" keyword.
        /// </summary>
        TargetedByAbility,
    }
}
