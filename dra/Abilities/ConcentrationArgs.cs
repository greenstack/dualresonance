﻿using DualResonance.Deployables.Units;
using DualResonance.Status;

namespace DualResonance.Abilities
{
    internal class ConcentrationArgs : ActivationArgs
    {
        internal StatModifier Change;

        public ConcentrationArgs(ActiveMercenary other, GameBoard board, StatModifier change) : base(other, board)
        {
            Change = change;
        }
    }
}
