﻿using DualResonance.Deployables;

namespace DualResonance.Abilities
{
    /// <summary>
    /// Represents an entity that can use two abilities.
    /// </summary>
    public interface IAbilityUser : IAbled
    {
        /// <summary>
        /// Causes the user to use their A ability on the target.
        /// </summary>
        /// <param name="user">The user of the ability.</param>
        /// <param name="target">The target of the ability.</param>
        void UseAbilityA(IActive target);

        /// <summary>
        /// Causes the user to use their B ability on the target.
        /// </summary>
        /// <param name="user">The user of the ability.</param>
        /// <param name="target">The target of the ability.</param>
        void UseAbilityB(IActive target);

        /// <summary>
        /// Ensures that the A ability can be used on the target.
        /// </summary>
        /// <param name="target">The target to test.</param>
        /// <returns>True if the A ability can be used on the target. Otherwise, false.</returns>
        bool VerifyValidTargetForA(IActive target);
        
        /// <summary>
        /// Ensures that the A ability can be used on the target.
        /// </summary>
        /// <param name="target">The target to test.</param>
        /// <returns>True if the A ability can be used on the target. Otherwise, false.</returns>
        bool VerifyValidTargetForB(IActive target);
    }
}
