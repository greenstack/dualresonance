﻿using System;
using DualResonance.Deployables;
using DualResonance.Deployables.Cards;
using DualResonance.Deployables.Units;
using DualResonance.Status;

namespace DualResonance.Abilities
{
    /// <summary>
    /// Contains all abilities that mercenaries and commanders can utilize.
    /// </summary>
    public static class AbilityList
    {
        private static readonly Random roller = new Random();

        /// <summary>
        /// Checks if the target is a Mercenary.
        /// </summary>
        /// <param name="target">The target being validated.</param>
        /// <returns>target is ActiveMercenary</returns>
        private static bool MercenaryCheck(IActive target)
        {
            return target is ActiveMercenary;
        }

        /// <summary>
        /// Checks the alliance of the teams.
        /// </summary>
        /// <param name="user">The user of the ability.</param>
        /// <param name="target">The target of the ability.</param>
        /// <param name="sameTeam">If the two actives should be on the same team or not.</param>
        /// <returns>True if the target's team is the correct one.</returns>
        public static bool TeamCheck(IActive user, IActive target, bool sameTeam = false)
        {
            // ! sameTeam XOR (ally and other are on the same team)
            return !(sameTeam ^ user.Team == target.Team);
        }

        public static readonly Ability Attack = new Ability
        {
            Name = "_merc_attack",
            VerifyValidTarget = (user, target) => TeamCheck(user, target),
            UseAbility = (user, target)
                => target.DecrementHealthPoints(((ActiveMercenary)user).AttackPower),
        };

        #region Standard abilities
        public static readonly Ability None = new Ability
        {
            Name = "---",
            Description = "",
            VerifyValidTarget = (user, target) => false,
            UseAbility = (user, target) => { },
        };

        /// <summary>
        /// Remove 2 HP from the caster. Prevent all damage to target merc until EOT.
        /// Todo: Implement this. This could be done by raising the target's Defense to absurd levels.
        /// </summary>
        public static readonly Ability AmberStasis = new Ability
        {
            Name = "Amber Stasis",
            Description = "Remove 2 HP from the caster. Prevent all damage to target merc until EOT.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target), //I suppose the player can target even enemies?
            UseAbility = (user, target) => throw new NotImplementedException("The Amber Stasis ability has yet to be implemented."),
            BarrierRequirement = 3,
        };

        public static readonly Ability Disable = new AbilityBuilder("Disable", "Testing this out")
            .SetDistortCost(1) 
            .Disable(AbilityTargets.Target | AbilityTargets.User, 1)
            .Finish();

        /// <summary>
        /// If target has WP &lt; user's WP, target merc loses 2 AP and is disabled until owner's next turn.
        /// </summary>
        public static readonly Ability Beguile = new Ability
        {
            Name = "Beguile",
            Description = "If target has WP < user's WP, target merc is disabled until the end of the owner's next turn.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                ActiveMercenary ally = (ActiveMercenary)user;
                ActiveMercenary merc = (ActiveMercenary)target;
                // What should I do if the test is failed?
                if (ally.WillPower > merc.WillPower)
                {
                    merc.disabled = new DisabledStatus(1);
                    merc.ModifyStats(0, -1, 1, ally);
                }
            },
            PsycheRequirement = 3,
        };

        /// <summary>
        /// Deals damage equal to half the user's WP.
        /// </summary>
        public static readonly Ability MindBlow = new Ability
        {
            Name = "Mind Blow",
            Description = "Deals damage equal to half the user's WP.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                target.DecrementHealthPoints((user as ActiveMercenary).WillPower / 2);
            },
            PsycheRequirement = 2,
        };

        /// <summary>
        /// The user drops a bomb on the target, dealing some extra damage.
        /// </summary>
        public static readonly Ability SmallPayload = new Ability
        {
            Name = "Small Payload",
            Description = "The user drops a bomb on the target, dealing some extra damage.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                ActiveMercenary ally = (ActiveMercenary)user;
                target.DecrementHealthPoints((int)Math.Max(ally.AttackPower * 1.25, ally.AttackPower + 1));
            },
            BurstRequirement = 2,
        };

        /// <summary>
        /// Deals damage to both HP and WP by half of the user's attack.
        /// </summary>
        public static readonly Ability SoulStrike = new Ability
        {
            Name = "Soul Strike",
            Description = "Deals damage to both health and willpower by half of the user's attack.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                int damage = (user as ActiveMercenary).AttackPower / 2;
                (target as ActiveMercenary).ModifyStats(0, damage, 1, user as ActiveMercenary);
                (target as ActiveMercenary).DecrementHealthPoints(damage);
            },
            PsycheRequirement = 2,
            BurstRequirement = 1,
        };

        /// <summary>
        /// Gives 1 AP and 1 WP to an ally for one turn.
        /// </summary>
        public static readonly Ability Encourage = new Ability
        {
            Name = "Encourage",
            Description = "Gives 1 AP and 1 WP to an ally for one turn.",
            VerifyValidTarget = (user, target) => TeamCheck(user, target, true) && MercenaryCheck(target),
            UseAbility = (user, target) => 
            {
                ActiveMercenary targM = target as ActiveMercenary;
                targM.ModifyStats(1, 1, 1, user as ActiveMercenary);
            },
            TriageRequirement = 1,
            PsycheRequirement = 1,
        };

        /// <summary>
        /// Inflicts -1 AP and -1 WP to an enemy for one turn.
        /// </summary>
        public static readonly Ability Discourage = new Ability
        {
            Name = "Discourage",
            Description = "Inflict -1 AP and -1 WP to an enemy for one turn.",
            VerifyValidTarget = (user, target) => TeamCheck(user, target) && MercenaryCheck(target),
            UseAbility = (user, target) => {
                ActiveMercenary targM = target as ActiveMercenary;
                targM.ModifyStats(-1, -1, 1, user as ActiveMercenary);
            },
            DistortRequirement = 1,
            TriageRequirement = 1,
        };

        /// <summary>
        /// Causes the target's next attack to inflict poison 1.
        /// </summary>
        public static readonly Ability HemlockBlessing = new Ability
        {
            Name = "Hemlock Blessing",
            Description = "Causes the target's next attack to inflict Poison 1.",
            VerifyValidTarget = (user, target) => TeamCheck(user, target, true) && MercenaryCheck(target),
            UseAbility = (user, target) => (target as ActiveMercenary).attack = _HemlockBlessingAtk,
            DistortRequirement = 1,
            TriageRequirement = 1
        };

        internal static readonly Ability _HemlockBlessingAtk = new Ability
        {
            Name = "_hemlockBlessingAtk",
            Description = "Causes the next attack to inflict poison 1.",
            VerifyValidTarget = (user, target) => TeamCheck(user, target),
            UseAbility = (user, target) =>
            {
                ActiveMercenary userM = user as ActiveMercenary;
                ActiveMercenary targM = target as ActiveMercenary;
                targM.Ailment = new Status.PoisonStatus(-1, 1, Status.StatusPrecedence.High);
                userM.attack = Attack;
                targM.DecrementHealthPoints(userM.AttackPower);
            }
        };

        /// <summary>
        /// Raises attack at the cost of willpower.
        /// </summary>
        public static readonly Ability Embroil = new Ability
        {
            Name = "Embroil",
            Description = "Raises attack at the cost of willpower.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                (target as ActiveMercenary).ModifyStats(2, -2, -1, user as ActiveMercenary);
            },
            BurstRequirement = 2,
        };

        /// <summary>
        /// Swaps the target's AP and WP.
        /// </summary>
        public static readonly Ability PowerSwitch = new Ability
        {
            Name = "Power Switch",
            Description = "Swaps the target's base AP and base WP.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                var targM = target as ActiveMercenary;
                int statDiff = targM.MaxAP - targM.MaxWP;
                targM.ModifyStats(-statDiff, statDiff, -1, user as ActiveMercenary);
            },
            DistortRequirement = 3
        };

        /// <summary>
        /// Removes all stat changes and statuses from the target.
        /// </summary>
        public static readonly Ability RefreshingAroma = new Ability
        {
            Name = "Refreshing Aroma",
            Description = "Removes stat changes and statuses from target.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                ActiveMercenary targM = target as ActiveMercenary;
                targM.ClearStatChanges(true, true);
                targM.Ailment = null;
                targM.Boon = null;
                targM.disabled = null;
            },
            TriageRequirement = 5,
        };

        /// <summary>
        /// Swaps the target's AP and WP, then drops both by 1.
        /// </summary>
        public static readonly Ability SilverMirror = new Ability
        {
            Name = "Silver Mirror",
            Description = "Swaps target's AP and WP, then drops both by 1.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                PowerSwitch.UseAbility(user, target);
                var targM = target as ActiveMercenary;
                targM.ModifyStats(-1, -1, -1, user as ActiveMercenary);
            },
            DistortRequirement = 4
        };

        /// <summary>
        /// Disable target unit. If they're holding an item, the unit steals it.
        /// </summary>
        public static readonly Ability Detain = new Ability
        {
            Name = "Detain",
            Description = "Disable target unit. If they're holding an item, the unit steals it.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            // TODO: Once items are implemented, this needs to be set.
            UseAbility = (user, target) => { (target as ActiveMercenary).disabled = new DisabledStatus(1); },
            PsycheRequirement = 4,
        };

        /// <summary>
        /// Roll user's WP + target's WP. If Max, target is destroyed. If 1, 
        /// user is destroyed. If even, target takes unit's AP in damage. If
        /// odd, unit takes target's AP in damage.
        /// </summary>
        public static readonly Ability Shootout = new Ability
        {
            Name = "Shootout",
            Description = "Roll user's WP + target's WP. If Max, target is destroyed. If 1, user is destroyed. If even, target takes unit's AP in damage." +
            " If odd, unit takes target's AP in damage.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (u, t) =>
            {
                var user = u as ActiveMercenary;
                var target = t as ActiveMercenary;
                int Max = user.WillPower + target.WillPower;
                int rollResult = roller.Next(1, Max + 1);
                if (rollResult == Max)
                    target.DecrementHealthPoints(target.MaxHP);
                else if (rollResult == 1)
                    user.DecrementHealthPoints(user.MaxHP);
                else if (rollResult % 2 == 0)
                    target.DecrementHealthPoints(user.AttackPower);
                else
                    user.DecrementHealthPoints(target.AttackPower);
            },
            BurstRequirement = 4,
        };

        #endregion Standard abilities

        #region Passive Abilities
        /// <summary>
        /// Vengeance: If unit's base WP > 1, respawn this unit with -1 base WP.
        /// </summary>
        public static readonly Ability Metalwrought = new PassiveAbility
        {
            Name = "Metalwrought",
            Description = "Vengeance: If unit's base WP > 1, respawn this unit with -1 base WP.",
            Trigger = AbilityTrigger.Destroyed,
            Activate = (unit, actArgs) =>
            {
                var session = actArgs.Board;
                // Create a card that is a copy, but with one less WP.
                MercenaryCard replacement = new MercenaryCard(unit.MaxHP, unit.MaxAP, unit.MaxWP - 1, 0, unit.MercenaryId)
                {
                    Name = unit.Name,
                    FlavorText = unit.FlavorText,
                    A = unit.A,
                    B = unit.B,
                };
                if (replacement.MaxWP == 0) return;

                ActiveMercenary newMerc = new ActiveMercenary(replacement, unit.Team);
                newMerc.OwnerPlayerId = unit.OwnerPlayerId;
                session.SpawnMercenary(unit.Location, newMerc);
            }
        };

        /// <summary>
        /// Retaliate: Deal damage equal to the unit's AP to the attacker.
        /// </summary>
        public static readonly Ability QuickCounter = new PassiveAbility
        {
            Name = "Quick Counter",
            Description = "Retaliate: Deal damage equal to the unit's AP to the attacker.",
            Trigger = AbilityTrigger.PostFoeAttack,
            Activate = (sender, actArgs) =>
            {
                actArgs.Other.DecrementHealthPoints(sender.AttackPower);
            }
        };

        /// <summary>
        /// Concentrate: Double the stat changes to the unit.
        /// </summary>
        public static readonly Ability ExtraMile = new PassiveAbility
        {
            Name = "Extra Mile",
            Description = "Concentrate: Double the stat changes to the unit.",
            Trigger = AbilityTrigger.StatModified,
            Activate = (sender, actArgs) =>
            {
                if (actArgs.Other == null) return;
                var conArgs = actArgs as ConcentrationArgs;
                // We don't pass in the actor here to prevent infinite loops/stack overflow issues from happening.
                conArgs.Change += conArgs.Change;
            }
        };

        /// <summary>
        /// Concentrate: Changes to AP and WP are done instead to WP and AP, respectively.
        /// </summary>
        public static readonly Ability CrossEyed = new PassiveAbility
        {
            Name = "Cross-Eyed",
            Description = "Concentrate: Changes to AP and WP are done instead to WP and AP, respectively.",
            Trigger = AbilityTrigger.StatModified,
            Activate = (sender, actArgs) =>
            {
                if (actArgs.Other == null) return;
                var conArgs = actArgs as ConcentrationArgs;
                conArgs.Change = new StatModifier(conArgs.Change.WPMod, conArgs.Change.APMod, conArgs.Change.Duration);
            }
        };

        /// <summary>
        /// Retaliate: Attacker takes 1 damage.
        /// </summary>
        public static readonly Ability PricklyArmor = new PassiveAbility
        {
            Name = "Prickly Armor",
            Description = "Retaliate: Attacker takes 1 damage.",
            Trigger = AbilityTrigger.PostFoeAttack,
            Activate = (sender, actArgs) =>
            {
                actArgs.Other.DecrementHealthPoints(1);
            }
        };

        /// <summary>
        /// Retaliate: Unit gains +1 AP.
        /// </summary>
        public static readonly Ability ShortFuse = new PassiveAbility
        {
            Name = "Short Fuse",
            Description = "Retaliate: Unit gains +1 AP.",
            Trigger = AbilityTrigger.PostFoeAttack,
            Activate = (sender, actArgs) =>
            {
                sender.ModifyStats(1, 0, -1);
            },
        };

        /// <summary>
        /// Feedback: Allied Invoker gains +1 WP.
        /// </summary>
        public static readonly Ability SharedKnowledge = new PassiveAbility
        {
            Name = "Shared Knowledge",
            Description = "Feedback: Allied Invoker gains +1 WP.",
            Trigger = AbilityTrigger.TargetedByAbility,
            Activate = (sender, actArgs) =>
            {
                if (actArgs.Other.Team == sender.Team)
                    actArgs.Other.ModifyStats(0, 1, -1, sender);
            },
        };

        /// <summary>
        /// Feedback: Foe invoker is disabled for 1 turn.
        /// </summary>
        public static readonly Ability UnjustPunishment = new PassiveAbility
        {
            Name = "Unjust Punishment",
            Description = "Feedback: Foe invoker is disabled for 1 turn.",
            Trigger = AbilityTrigger.TargetedByAbility,
            Activate = (sender, actArgs) =>
            {
                if (actArgs.Other.Team != sender.Team)
                    actArgs.Other.disabled = new DisabledStatus(1);
            }
        };
        #endregion Passive Abilities

        #region Test abilities
#if !DEMO
        /// <summary>
        /// A simple test ability that is always invalid and does nothing.
        /// </summary>
        public static readonly Ability TEST_AlwaysInvalid = new Ability
        {
            Name = "TEST AlwaysInvalid",
            Description = "A simple test ability that is always invalid and does nothing.",
            VerifyValidTarget = (user, target) => false,
            UseAbility = (user, target) => { }
        };

        /// <summary>
        /// When used on an ITickableCard, that card's Tick() method is called.
        /// </summary>
        public static readonly Ability TEST_ForceTick = new Ability
        {
            Name = "TEST ForceTick",
            Description = "When used on an ITickableCard, that card's Tick() method is called.",
            VerifyValidTarget = (user, target) => target is ITick,
            UseAbility = (user, target) => target.Tick()
        };

        /// <summary>
        /// A simple test ability that removes the target card from play immediately.
        /// </summary>
        public static readonly Ability TEST_RemoveImmediately = new Ability
        {
            Name = "TEST RemoveImmediately",
            Description = "A simple test ability that removes the target card from play immediately.",
            VerifyValidTarget = (user, target) => true,
            UseAbility = (user, target) => target.RemoveFromPlay(RemovalCause.Killed)
        };

        public static readonly Ability TEST_Refresh = new Ability
        {
            Name = "TEST Refresh",
            Description = "A simple test ability that causes that target Mercenary to be healed of an ailment.",
            VerifyValidTarget = (user, target) => MercenaryCheck(target),
            UseAbility = (user, target) =>
            {
                (target as ActiveMercenary).Ailment = null;
            }
        };
#endif
#endregion Test abilities
    }
}
