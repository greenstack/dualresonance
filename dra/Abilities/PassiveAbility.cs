﻿using DualResonance.Deployables.Units;

namespace DualResonance.Abilities
{
    public class PassiveAbility : Ability
    {
        /// <summary>
        /// Identifies when the ability can be triggered.
        /// </summary>
        public AbilityTrigger Trigger { get; internal set; }
        // This matches the object sender and event args pattern.
        public ActiveMercenary.OnReact Activate;

        public PassiveAbility()
        {
            VerifyValidTarget = (user, target) => false;
            UseAbility = (user, target) => { };
        }
    }
}
