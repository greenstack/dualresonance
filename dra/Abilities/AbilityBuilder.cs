﻿using DualResonance.Deployables;
using DualResonance.Deployables.Units;
using DualResonance.Status;

namespace DualResonance.Abilities
{
    /// <summary>
    /// Builds an ability by calling functions on the instance.
    /// </summary>
    public class AbilityBuilder
    {
        private Ability built;
        private AbilityTags tags;
        private bool building = true;

        public AbilityBuilder(string name, string flavorText)
        {
            built = new Ability()
            {
                Name = name,
                FlavorText = flavorText,
                VerifyValidTarget = (user, target) => false,
                UseAbility = (user, target) => { }
            };
        }

        /// <summary>
        /// Ensures that the ability isn't currently being built.
        /// </summary>
        private void AssertUnfinished()
        {
            if (!building)
                throw new System.InvalidOperationException("Cannot modify ability after Finish has been called.");
        }

        internal static bool MercenaryCheck(IActive target)
        {
            return target is ActiveMercenary;
        }

        internal static bool TeamCheck(IActive user, IActive target, bool sameTeam = false)
        {
            // ! sameTeam XOR (ally and other are on the same team)
            return !(sameTeam ^ user.Team == target.Team);
        }

        public AbilityBuilder SetVerification(AbilityVerification verification)
        {
            switch (verification)
            {
                case AbilityVerification.Mercenary:
                    built.VerifyValidTarget = (user, target) => MercenaryCheck(target);
                    tags |= AbilityTags.TargetsMerc;
                    break;
                case AbilityVerification.OtherTeam:
                    built.VerifyValidTarget = (user, target) => TeamCheck(user, target);
                    tags |= AbilityTags.TargetsEnemy;
                    break;
                case AbilityVerification.SameTeam:
                    built.VerifyValidTarget = (user, target) => TeamCheck(user, target, true);
                    tags |= AbilityTags.TargetsAlly;
                    break;
                case AbilityVerification.Always:
                    built.VerifyValidTarget = (user, target) => true;
                    tags |= AbilityTags.TargetsAll;
                    break;
                case AbilityVerification.Never:
                    built.VerifyValidTarget = (user, target) => false;
                    // Don't set any targeting flags. The lack of them suffices.
                    break;
                default:
                    throw new System.InvalidOperationException("Invalid verification provided.");
            }
            return this;
        }

        #region Resonance Cost Settings
        /// <summary>
        /// Sets the barrier cost of the ability.
        /// </summary>
        /// <param name="cost">The amount of barrier resonance to charge.</param>
        /// <returns>The AbilityBuilder for chaining.</returns>
        public AbilityBuilder SetBarrierCost(int cost)
        {
            AssertUnfinished();
            built.BarrierRequirement = cost;
            tags |= AbilityTags.Barrier;
            return this;
        }

        /// <summary>
        /// Sets the burst cost of the ability.
        /// </summary>
        /// <param name="cost">The amount of burst resonance to charge.</param>
        /// <returns>The AbilityBuilder for chaining.</returns>
        public AbilityBuilder SetBurstCost(int cost)
        {
            AssertUnfinished();
            built.BurstRequirement = cost;
            tags |= AbilityTags.Burst;
            return this;
        }

        /// <summary>
        /// Sets the psyche cost of the ability.
        /// </summary>
        /// <param name="cost">The amount of psyche resonance to charge.</param>
        /// <returns>The AbilityBuilder for chaining.</returns>
        public AbilityBuilder SetPsycheCost(int cost)
        {
            AssertUnfinished();
            built.PsycheRequirement = cost;
            tags |= AbilityTags.Psyche;
            return this;
        }

        /// <summary>
        /// Sets the distort cost of the ability.
        /// </summary>
        /// <param name="cost">The amount of distort resonance to charge.</param>
        /// <returns>The AbilityBuilder for chaining.</returns>
        public AbilityBuilder SetDistortCost(int cost)
        {
            AssertUnfinished();
            built.DistortRequirement = cost;
            tags |= AbilityTags.Distort;
            return this;
        }

        /// <summary>
        /// Sets the triage cost of the ability.
        /// </summary>
        /// <param name="cost">The amount of triage resonance to charge.</param>
        /// <returns>The AbilityBuilder for chaining.</returns>
        public AbilityBuilder SetTriageCost(int cost)
        {
            AssertUnfinished();
            built.TriageRequirement = cost;
            tags |= AbilityTags.Triage;
            return this;
        }
        #endregion Resonance Cost Settings

        /// <summary>
        /// Causes the ability to disable the specified targets.
        /// </summary>
        /// <param name="targets">The units that should be targeted.</param>
        /// <param name="duration">How long the disabling effect should last.</param>
        /// <returns>The ability builder.</returns>
        public AbilityBuilder Disable(AbilityTargets targets, int duration)
        {
            AssertUnfinished();
            if (targets.HasFlag(AbilityTargets.Target))
            {
                built.UseAbility += (user, target) =>
                {
                    (target as ActiveMercenary).disabled = new DisabledStatus(duration);
                };
                tags |= AbilityTags.DisablesTarget;
            }
            if (targets.HasFlag(AbilityTargets.User))
            {
                built.UseAbility += (user, target) =>
                {
                    (user as ActiveMercenary).disabled = new DisabledStatus(duration);
                };
                tags |= AbilityTags.DisablesUnit;
            }
            tags |= AbilityTags.Disables;
            return this;
        }

        public AbilityBuilder ModifyStats(AbilityTargets targets, int apMod, int wpMod, int duration)
        {
            AssertUnfinished();
            if (targets.HasFlag(AbilityTargets.Target))
            {
                built.UseAbility += (user, target) =>
                {
                    var tMerc = target as ActiveMercenary;
                    tMerc.ModifyStats(apMod, wpMod, duration, (ActiveMercenary)user);
                };
                tags |= AbilityTags.ChangesStatsOnTarget;
            }
            if (targets.HasFlag(AbilityTargets.User))
            {
                built.UseAbility += (user, target) =>
                {
                    var uMerc = user as ActiveMercenary;
                    uMerc.ModifyStats(apMod, wpMod, duration, (ActiveMercenary)user);
                };
                tags |= AbilityTags.ChangesStatsOnTarget;
            }
            tags |= AbilityTags.ChangesStats;
            return this;
        }

        /// <summary>
        /// Retrieves the ability that was built.
        /// </summary>
        /// <returns>The built ability.</returns>
        public Ability Finish()
        {
            building = false;
            return built;
        }
    }
}
