﻿namespace DualResonance.Commands
{
    class EndTurnCommand : AbstractCommand
    {
        public EndTurnCommand(IPlayer actor) : base(actor)
        {
        }

        public override bool Apply(GameSession context, out string message)
        {
            message = "Ending turn";
            if (context.ActivePlayer == actor)
            {
                context.EndTurn();
                return true;
            }
            return false;
        }
    }
}
