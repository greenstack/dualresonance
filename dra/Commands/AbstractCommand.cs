﻿namespace DualResonance.Commands
{
    /// <summary>
    /// The base class for all commands.
    /// </summary>
    public abstract class AbstractCommand : IGameCommand
    {
        protected readonly IPlayer actor;

        protected const string SuccessMsg = "The command completed successfully.";

        public AbstractCommand(IPlayer actor)
        {
            this.actor = actor;
        }
        /// <summary>
        /// Attempts to apply the command to the provided context.
        /// </summary>
        /// <param name="context">The game board to apply the command to.</param>
        /// <param name="message">A message describing either the status.</param>
        /// <returns>True if the command was applied correctly. Otherwise, false.</returns>
        public abstract bool Apply(GameSession context, out string message);
    }
}
