﻿using System;

namespace DualResonance.Commands
{
    public class MoveCommand : AbstractCommand
    {
        private readonly BoardCoordinate location;
        private readonly Direction d;

        public MoveCommand(IPlayer actor, BoardCoordinate location, Direction d) : base(actor)
        {
            this.location = location;
            this.d = d;
        }

        public override bool Apply(GameSession context, out string message)
        {
            try
            {
                var destination = location + BoardCoordinate.FromDirection(d);
                context.MoveUnit(actor, location, destination);

                message = SuccessMsg;
                return true;
            }
            catch (InvalidDRActionException ioe)
            {
                message = ioe.Message;
                return false;
            }
        }
    }
}
