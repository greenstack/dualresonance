﻿using System;

namespace DualResonance.Commands
{
    /// <summary>
    /// A command that will make a mercenary at a target location either
    /// use an ability or an attack.
    /// </summary>
    public class InvokeCommand : AbstractCommand
    {
        private readonly AbilityId abilityId;
        private readonly BoardCoordinate location;
        private readonly Direction d;
        /// <summary>
        /// Creates a new InvokeCommand.
        /// </summary>
        /// <param name="actor">The player attempting to execute the command.</param>
        /// <param name="id">The id of the ability to be invoked.</param>
        /// <param name="mercLocation">The location of the merc that should move.</param>
        /// <param name="d">The direction in which the unit should move.</param>
        public InvokeCommand(
            IPlayer actor,
            BoardCoordinate mercLocation,
            Direction d,
            AbilityId id) : base(actor)
        {
            abilityId = id;
            location = mercLocation;
            this.d = d;
        }

        public override bool Apply(GameSession context, out string message)
        {
            try
            {
                switch (abilityId)
                {
                    case AbilityId.Attack:
                        context.InvokeAttack(actor, location, d);
                        break;
                    default:
                        context.InvokeAbility(actor, location, d, abilityId);
                        break;
                }
                message = SuccessMsg;
                return true;
            }
            catch (InvalidDRActionException ioe)
            {
                message = ioe.Message;
                return false;
            }  
        }
    }
}
