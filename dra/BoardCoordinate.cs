﻿namespace DualResonance
{
    /// <summary>
    /// Represents a location on the board. Down on the Y-axis is positive.
    /// </summary>  
    public struct BoardCoordinate
    {
        public static BoardCoordinate OffBoard = new BoardCoordinate(-1, -1);

        public int X;
        public int Y;

        public BoardCoordinate(int x, int y)
        {
            X = x; Y = y;
        }

        /// <summary>
        /// Determines the direction that the other coordinate lies in.
        /// </summary>
        /// <param name="other">The coordinate to check against.</param>
        /// <returns>The direction in which the other coordinate resides.</returns>
        public Direction DirectionTo(BoardCoordinate other)
        {
            Direction finalDirection = Direction.None;
            // all these checks are necessary to provide the correct answer.
            if (other.X < X)
                finalDirection |= Direction.West;
            else if (other.X > X)
                finalDirection |= Direction.East;

            if (other.Y < Y)
                finalDirection |= Direction.North;
            else if (other.Y > Y)
                finalDirection |= Direction.South;

            return finalDirection;
        }

        /// <summary>
        /// Gets the distance from this board coordinate to the other, based
        /// on 4-distance.
        /// </summary>
        /// <param name="other">The other coordinate to measure.</param>
        /// <returns>The 4-neighbor distance to the other location.</returns>
        public int DistanceTo(BoardCoordinate other)
        {
            int xDiff, yDiff;
            xDiff = System.Math.Abs(X - other.X);
            yDiff = System.Math.Abs(Y - other.Y);
            return xDiff + yDiff;
        }

        /// <summary>
        /// Creates a board coordinate pointing in a certain direction.
        /// </summary>
        /// <param name="direction">The direction</param>
        /// <returns>A board coordinate pointing in the provided direction.</returns>
        public static BoardCoordinate FromDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return new BoardCoordinate(0, -1);
                case Direction.South:
                    return new BoardCoordinate(0, 1);
                case Direction.East:
                    return new BoardCoordinate(1, 0);
                case Direction.West:
                    return new BoardCoordinate(-1, 0);
                default:
                    throw new System.ArgumentException("An invalid direction was provided.");
            }
        }
            
        public static BoardCoordinate operator+(BoardCoordinate first, BoardCoordinate second)
        {
            return new BoardCoordinate(first.X + second.X, first.Y + second.Y);
        }

        public static BoardCoordinate operator -(BoardCoordinate first, BoardCoordinate second)
        {
            return new BoardCoordinate(first.X - second.X, first.Y - second.Y);
        }

        public static bool operator ==(BoardCoordinate lhs, BoardCoordinate rhs)
        {
            return lhs.X == rhs.X && lhs.Y == rhs.Y;
        }

        public static bool operator !=(BoardCoordinate lhs, BoardCoordinate rhs)
        {
            return lhs.X != rhs.X && lhs.Y != rhs.Y;
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is BoardCoordinate))
            {
                return false;
            }

            var coordinate = (BoardCoordinate)obj;
            return X == coordinate.X &&
                   Y == coordinate.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}