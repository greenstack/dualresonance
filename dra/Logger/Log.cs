﻿using System.Collections.Generic;

namespace DualResonance.Logger
{
    public class Log
    {
        public static Log Instance = new Log();

        readonly List<LogMessage> log = new List<LogMessage>();

        /// <summary>
        /// Retrieves a number of log messages from the back of the log.
        /// </summary>
        /// <param name="count">The number of items to get. A number less than one yields the entire log.</param>
        /// <returns>The last n log messages.</returns>
        public List<LogMessage> GetItems(int count = 0)
        {
            if (count <= 0) return log;
            if (count >= log.Count)
                return log;
            return log.GetRange(log.Count - count, count);
        }

        public void RegisterEvent(LogMessage action) {
            log.Add(action);
        }

        public void ClearLog()
        {
            log.Clear();
        }
    }
}
