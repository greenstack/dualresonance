﻿using DualResonance.Deployables.Units;

namespace DualResonance.Logger
{
    class AbilityUsedMessage : LogMessage
    {
        public readonly ActiveBase invoker;
        public readonly ActiveBase target;
        public readonly Abilities.Ability ability;

        public AbilityUsedMessage(ActiveBase invoker, ActiveBase target, Abilities.Ability ability)
        {
            this.invoker = invoker;
            this.target = target;
            this.ability = ability;
        }

        public override string ToString()
        {
            return $"{invoker.Name} used {ability.Name} on {target.Name}";
        }
    }
}
