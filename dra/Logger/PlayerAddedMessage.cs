﻿namespace DualResonance.Logger
{
    class PlayerAddedMessage : LogMessage
    {
        private readonly Player player;
        
        public PlayerAddedMessage(Player player)
        {
            this.player = player;
        }

        public override string ToString()
        {
            return $"Player {player.PlayerId} was added to the game!";
        }
    }
}
