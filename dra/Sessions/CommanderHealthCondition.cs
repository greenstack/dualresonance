﻿using System;
using System.Collections.Generic;
using DualResonance.Utilities;

namespace DualResonance.Sessions
{
    public sealed class CommanderHealthCondition : Condition
    {
        private int playerIdToCheck;

        private int threshHold;

        private readonly Func<int, int, bool> comparisonOperator;

        public CommanderHealthCondition(int playerId, int limit, Teams winningTeam, Comparer c, bool invert) : base(winningTeam, invert)
        {
            playerIdToCheck = playerId;
            threshHold = limit;
            comparisonOperator = Comparisons.FromComparer(c);
        }

        protected override bool SpecificCheck(GameSession session)
        {
            return comparisonOperator(session.GetPlayerCommander(playerIdToCheck).HealthPoints, threshHold);
        }

        public override string ToString()
        {
            return $"Defeat the enemy commander.";
        }
    }
}
