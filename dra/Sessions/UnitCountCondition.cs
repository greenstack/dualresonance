﻿using DualResonance.Utilities;
using System;
using System.Linq;

namespace DualResonance.Sessions
{
    public class UnitCountCondition : Condition
    {
        Teams teamToCheck;
        int unitsToCount;
        private readonly Func<int, int, bool> comparisonOperator;

        public UnitCountCondition(int unitCount, Teams checkTeam, Teams winningTeam, Comparer c, bool invert) : base(winningTeam, invert)
        {
            unitsToCount = unitCount;
            teamToCheck = checkTeam;
            comparisonOperator = Comparisons.FromComparer(c);
        }

        protected override bool SpecificCheck(GameSession session)
        {
            return comparisonOperator(session.Board.GetMercsOnTeam(teamToCheck).Count(), unitsToCount);
        }

        public override string ToString()
        {
            return $"The enemy must have {unitsToCount} units remaining.";
        }
    }
}
