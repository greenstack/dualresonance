﻿using System.Collections.Generic;

namespace DualResonance.Sessions
{
    public abstract class Condition : ICondition
    {
        public Teams Winner { get; private set; }

        public Condition(Teams winningTeam, bool invert)
        {
            Winner = winningTeam;
            InvertCondition = invert;
        }

        public bool InvertCondition { get; private set; }

        protected abstract bool SpecificCheck(GameSession session);

        /// <summary>
        /// Determines if the circumstances of this win condition have been met.
        /// </summary>
        /// <param name="session">The current game session.</param>
        /// <returns>True if the win condition has been met.</returns>
        public bool ConditionMet(GameSession session)
        {
            return !(SpecificCheck(session) ^ !InvertCondition);
        }
    }
}
