﻿namespace DualResonance
{
    public class Player : IPlayer
    {
        public Teams Team { get; set; }
        public int PlayerId { get; set; }
        public DeckInfo CurrentDeck { get; set; }
    }
}
