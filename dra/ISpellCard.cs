﻿using System;

namespace DualResonance
{
    public interface ISpellCard : INamed
    {
        /// <summary>
        /// The effect that this card has.
        /// </summary>
        Action Effect { get; set; }
    }
}
