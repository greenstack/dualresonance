﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DualResonance
{
    /// <summary>
    /// The teams.
    /// </summary>
    public enum Teams
    {
        /// <summary>
        /// For any active unit without a team.
        /// </summary>
        Neutral,
        /// <summary>
        /// Shows that the card is on team alpha.
        /// </summary>
        Alpha,
        /// <summary>
        /// Shows that the card is on team beta.
        /// </summary>
        Beta,
    }
}
