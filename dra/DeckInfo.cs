﻿using System;
using System.Collections.Generic;

using DualResonance.Deployables.Cards;

namespace DualResonance
{
    /// <summary>
    /// Represents a deck that a player can bring into battle.
    /// </summary>
    public class DeckInfo
    {
        public const int MERCENARY_COUNT = 8;

        /// <summary>
        /// Maps to the mercenary as found in MercenaryCards.
        /// </summary>
        public List<int> MercenaryIds = new List<int>(MERCENARY_COUNT);

        public CommanderCard Commander { get; set; }

        public int UnitsInDeck
        {
            get => MercenaryIds.Count;
        }

        /// <summary>
        /// Adds the mercenary id to the deck, if it's a valid one.
        /// </summary>
        /// <param name="mercenary_id">The ID of the mercenary.</param>
        public void AddMercenary(int mercenary_id)
        {
            if (MercenaryIds.Count == MERCENARY_COUNT)
                throw new CollectionFullException("There are too many mercenaries in the deck to add another.");
            else if (MercenaryCards.GetMercenary(mercenary_id) == null)
                // Change this exception type
                throw new ArgumentOutOfRangeException("The mercenary from the id doesn't exist.");
            MercenaryIds.Add(mercenary_id);

        }

        /// <summary>
        /// Removes the mercenary in the specific position from the deck.
        /// </summary>
        /// <param name="position">The position in the Merc Decklist to remove the mercenary from.</param>
        public void RemoveMercenary(int position)
        {
            MercenaryIds.RemoveAt(position);
        }

        /// <summary>
        /// Gets the mercenary card info from the Mercenary decklist.
        /// </summary>
        /// <param name="position">The position in the Mercenary Decklist of the mercenary to remove.</param>
        /// <returns>A Mercenary Card reference.</returns>
        public MercenaryCard GetMercenary(int position)
        {
            if (position >= MERCENARY_COUNT)
                throw new ArgumentOutOfRangeException("There is no mercenary at the specified position.");
            else if (position >= MercenaryIds.Count)
                return null;
            else return MercenaryCards.Mercenaries[MercenaryIds[position]];
        }

        /// <summary>
        /// Ensures that the deck is a valid one.
        /// </summary>
        /// <param name="message">A message regarding the validation.</param>
        /// <returns>True if the deck is valid.</returns>
        public bool Validate(out string message)
        {
            // All test cases passed
            message = null;
            return true;
        }
    }
}
