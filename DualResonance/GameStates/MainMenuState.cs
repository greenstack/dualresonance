﻿using DualResonance.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance.GameStates
{
    /// <summary>
    /// The main menu game state.
    /// </summary>
    class MainMenuState : AbstractGameState
    {
        Menu<string> menu;

        public MainMenuState()
        {
            menu = new Menu<string>(new string[]{
                "New Game",
                "Load Game",
                "Options",
                "Quit"
            });
        }

        // Menu Options:
        // - New
        // - Load
        // - Options
        // - Quit
        public override void Draw(Game1 game, GameTime gameTime)
        {
            AssetManager manager = AssetManager.Instance;
            float offset = 51.2f;
            float xpos = 100;
            // New Game
            manager.DrawStatIcon(StatIcon.BurstResonance, new Vector2(100, xpos), menu.IsSelected(0) ? null : (Color?)Color.Gray, .05f);
            // Load
            manager.DrawStatIcon(StatIcon.PsycheResonance, new Vector2(100, xpos += offset), menu.IsSelected(1) ? null : (Color?)Color.Gray, .05f);
            // Options
            manager.DrawStatIcon(StatIcon.TriageResonance, new Vector2(100, xpos += offset), menu.IsSelected(2) ? null : (Color?)Color.Gray, .05f);
            // Quit
            manager.DrawStatIcon(StatIcon.DistortResonance, new Vector2(100, xpos += offset), menu.IsSelected(3) ? null : (Color?)Color.Gray, .05f);
        }

        /// <summary>
        /// Updates the state of the game.
        /// </summary>
        /// <param name="game">The game whose state is being updated.</param>
        /// <param name="gameTime"></param>
        public override void Update(Game1 game, GameTime gameTime)
        {
            if (InputManager.IsDiscreteDown(Keys.Down))
                menu.MoveToNext();
            else if (InputManager.IsDiscreteDown(Keys.Up))
                menu.MoveToPrevious();
            else if (InputManager.IsDiscreteDown(Keys.Z) || InputManager.IsDiscreteDown(Keys.Enter))
            {
                if (menu.IsSelected(0)) game.State = new WorldMapState();//new InLevelState(game, "copper1.drl");
                else if (menu.IsSelected(1)) ; // Load
                else if (menu.IsSelected(2)) ; // Options
                else if (menu.IsSelected(3)) game.Exit();
            }
        }
    }
}
