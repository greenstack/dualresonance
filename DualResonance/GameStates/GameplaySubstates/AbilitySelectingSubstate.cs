﻿using DualResonance.Deployables.Units;
using DualResonance.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace DualResonance.GameStates.GameplaySubstates
{
    class AbilitySelectingSubstate : MenuSubState<int>
    {
        
        public static int[] prepMenu(ActiveMercenary unit)
        {
            List<int> options = new List<int>();
            if (unit.A != null && unit.A.Name != "---" && !(unit.A is Abilities.PassiveAbility))
                options.Add((int)AbilityId.A);
            if (unit.B != null && unit.B.Name != "---" && !(unit.B is Abilities.PassiveAbility))
                options.Add((int)AbilityId.B);
            options.Add(-1);
            return options.ToArray();
        }

        public AbilitySelectingSubstate(ActiveMercenary unit) : base(prepMenu(unit)) 
        {
            List<string> options = new List<string>();
            if (unit.A != null && unit.A.Name != "---")
                options.Add(unit.A.Name);
            if (unit.B != null && unit.B.Name != "---")
                options.Add(unit.B.Name);
            options.Add("Back");

            Overwrite = options.ToArray();
        }

        protected override void HandleMenuSelection(InLevelState superState)
        {
            AbilityId id = AbilityId.A;
            switch (options.SelectedItem)
            {
                case (int)AbilityId.B:
                    id = AbilityId.B;
                    goto default;
                case -1:
                    superState.subState = new FreeCursorSubstate();
                    break;
                case (int)AbilityId.A:
                default:
                    superState.subState = new InvokeUnitAbilitySubstate(id, superState.cursor);
                    break;
            }
        }

        public override void Draw(InLevelState superState, GameTime gameTime)
        {
            base.Draw(superState, gameTime);

            if (options.SelectedItem == -1)
                return;
            string abDesc = (AbilityId)options.SelectedItem == AbilityId.A ?
                superState.level.Query(superState.cursor).A.Description:
                superState.level.Query(superState.cursor).B.Description;
            abDesc = AssetManager.Instance.WrapText(abDesc, 200);
            AssetManager.Instance.WriteString(abDesc, new Vector2(300, 400), Color.Black);
        }
    }
}
