﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance.GameStates.GameplaySubstates
{
    abstract class LimitedMovementSubstate : AbstractLevelSubstate, IExtendCursorState
    {
        public BoardCoordinate OriginalLocation { get; set; }

        public LimitedMovementSubstate(BoardCoordinate originalLocation)
        {
            OriginalLocation = originalLocation;
        }

        // This should only display the info of the unit that's been selected.
        public override void Draw(InLevelState superState, GameTime gameTime)
        {

        }

        // This should limit movement in one direction from the selected spot.
        public override void Update(InLevelState superState, GameTime gameTime)
        {
            BoardCoordinate original = new BoardCoordinate(superState.cursor.X, superState.cursor.Y);

            HandleCursorMovement(superState);
            // For now, let's not allow movement of more than one space.
            if (superState.cursor.DistanceTo(OriginalLocation) > Deployables.Units.ActiveBase.MAX_MOVEMENT)
            {
                superState.cursor = original;
                return;
            }

            if (InputManager.IsDiscreteDown(Keys.Enter) || InputManager.IsDiscreteDown(Keys.Z))
            {
                // Check if the action is correctly performed. If it is, change the state;
                // otherwise, change nothing.
                if (superState.cursor == OriginalLocation)
                {
                    superState.subState = new FreeCursorSubstate();
                    return;
                }
                try
                {
                    TakeAction(superState, superState.player);
                    superState.subState = new FreeCursorSubstate();
                }
                catch (InvalidDRActionException e)
                {
                    superState.RegisterTemporaryMessage(e.Message);
                }
            }
            else if (InputManager.IsDiscreteDown(Keys.X) || InputManager.IsDiscreteDown(Keys.Back))
                superState.subState = new FreeCursorSubstate();
        }

        protected abstract void TakeAction(InLevelState superState, IPlayer player);
    }
}
