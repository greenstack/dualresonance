﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance.GameStates.GameplaySubstates
{
    abstract class AbstractLevelSubstate
    {
        /// <summary>
        /// Draws any extra components associated with this state above
        /// what the super state draws.
        /// </summary>
        /// <param name="superState">The state this lives in.</param>
        /// <param name="gameTime">The time elapsed since last draw.</param>
        public abstract void Draw(InLevelState superState, GameTime gameTime);
        /// <summary>
        /// Updates the level state.
        /// </summary>
        /// <param name="superState">The level state.</param>
        /// <param name="gameTime">The time elapsed since the last update.</param>
        public abstract void Update(InLevelState superState, GameTime gameTime);

        protected void HandleCursorMovement(InLevelState superState)
        {
            BoardCoordinate original = new BoardCoordinate(superState.cursor.X, superState.cursor.Y);

            if (InputManager.IsDiscreteDown(Keys.Down))
                superState.cursor.Y++;
            else if (InputManager.IsDiscreteDown(Keys.Up))
                superState.cursor.Y--;
            else if (InputManager.IsDiscreteDown(Keys.Right))
                superState.cursor.X++;
            else if (InputManager.IsDiscreteDown(Keys.Left))
                superState.cursor.X--;

            if (!superState.level.IsInBounds(superState.cursor))
                superState.cursor = original;
            // This should only be done in attacking substate.
            /*if (!superState.level.IsInBounds(superState.cursor)
                && original.DirectionTo(superState.cursor) != Direction.North
                && superState.level.PlayerCommanders[1] != null)
                superState.cursor = original;*/
        }
    }
}
