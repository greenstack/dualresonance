﻿using DualResonance.Deployables.Units;
using System;

namespace DualResonance.GameStates.GameplaySubstates
{
    /// <summary>
    /// This state is used in levels when an allied unit has been selected.
    /// It allows the player to select what the unit should do.
    /// </summary>
    class AllySelectedSubstate : MenuSubState<string>
    {
        private readonly string[] canMove = new string[]
        {
            "Move",
            "Attack",
            "Ability",
            "Rest",
            "Back"
        };
        
        private static string[] makeMenu(bool canMove, bool adjacentToCommander)
        {
            string[] menu = new string[]
            {
                "Attack",
                "Ability",
                "Rest",
                "Back"
            };
            if (adjacentToCommander)
            {
                string[] newMenu = new string[menu.Length + 1];
                newMenu[1] = "Att. Cmdr";
                newMenu[0] = menu[0];
                Array.Copy(menu, 1, newMenu, 2, 3);
                menu = newMenu;
            }
            if (canMove)
            {
                string[] newMenu = new string[menu.Length + 1];
                newMenu[0] = "Move";
                Array.Copy(menu, 0, newMenu, 1, menu.Length);
                menu = newMenu;
            }

            return menu;
        }

        public AllySelectedSubstate(bool canMove, bool adjacentToCommander) 
            : base(makeMenu(canMove, adjacentToCommander))
        {
            
        }

        protected override void HandleMenuSelection(InLevelState superState)
        {
            switch (options.SelectedItem)
            {
                case "Move":
                    // Move
                    superState.subState = new MovingUnitState(superState.cursor);
                    break;
                case "Attack":
                    // Attack
                    superState.subState = new AttackingUnitSubstate(superState.cursor);
                    break;
                case "Att. Cmdr":
                    superState.level.InvokeAttack(superState.player, superState.cursor, Direction.North);
                    goto case "Back";
                case "Ability":
                    // Ability
                    superState.subState = new AbilitySelectingSubstate(superState.level.Query(superState.cursor) as ActiveMercenary);
                    break;
                case "Rest":
                    // Rest
                    superState.level.Rest(superState.player, superState.cursor);
                    goto case "Back";
                case "Back":
                    // Back
                    superState.subState = new FreeCursorSubstate();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("Invalid selected index.");
            }
        }
    }
}
