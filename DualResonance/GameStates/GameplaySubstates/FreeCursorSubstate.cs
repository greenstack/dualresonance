﻿using DualResonance.Deployables;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance.GameStates.GameplaySubstates
{
    class FreeCursorSubstate : AbstractLevelSubstate
    {
        public override void Draw(InLevelState superState, GameTime gameTime)
        {
            // There's nothing extra really to do here
        }

        public override void Update(InLevelState superState, GameTime gameTime)
        {
            if (InputManager.IsDiscreteDown(Keys.Enter) || InputManager.IsDiscreteDown(Keys.Z))
            {
                var merc = superState.level.Query(superState.cursor);
                if (merc == null || !merc.CanAct || merc.Team != Teams.Alpha)
                {
                    superState.subState = new EmptySpaceSelectedSubstate(superState);
                }
                else if ((merc as ILocatable) != null)
                {
                    var lMerc = merc as ILocatable;
                    superState.subState = new AllySelectedSubstate(lMerc.CanMove, lMerc.Location.Y == 0);
                }
                return;
            }

            HandleCursorMovement(superState);
        }
    }
}
