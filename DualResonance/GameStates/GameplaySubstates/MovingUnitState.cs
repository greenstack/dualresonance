﻿namespace DualResonance.GameStates.GameplaySubstates
{
    class MovingUnitState : LimitedMovementSubstate
    {
        public MovingUnitState(BoardCoordinate originalLocation) : base(originalLocation)
        {
        }

        protected override void TakeAction(InLevelState superState, IPlayer player)
        {
            superState.level.MoveUnit(player, OriginalLocation, superState.cursor);
        }
    }
}
