﻿using DualResonance.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance.GameStates.GameplaySubstates
{
    abstract class MenuSubState<T> : AbstractLevelSubstate
    {
        protected Menu<T> options;

        protected string[] Overwrite;

        public MenuSubState(T[] mappings)
        {
            options = new Menu<T>(mappings);
        }

        public override void Draw(InLevelState superState, GameTime gameTime)
        {
            // Draw the menu (be sure to highlight the selected option)
            AssetManager manager = AssetManager.Instance;
            // Right-aligned to the screen
            manager.RenderMenu(options, Overwrite);
        }

        public override void Update(InLevelState superState, GameTime gameTime)
        {
            // Only update the menu
            if (InputManager.IsDiscreteDown(Keys.Down))
                options.MoveToNext();
            else if (InputManager.IsDiscreteDown(Keys.Up))
                options.MoveToPrevious();
            else if (InputManager.IsDiscreteDown(Keys.Enter) || InputManager.IsDiscreteDown(Keys.Z))
            {
                HandleMenuSelection(superState);
            }
            else if (InputManager.IsDiscreteDown(Keys.X) || InputManager.IsDiscreteDown(Keys.Back))
            {
                superState.subState = new FreeCursorSubstate();
            }
        }

        protected abstract void HandleMenuSelection(InLevelState superState);
    }
}
