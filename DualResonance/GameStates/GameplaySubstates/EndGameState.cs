﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance.GameStates.GameplaySubstates
{
    class EndGameState : MenuSubState<string>
    {
        readonly Game1 gameRef;
        readonly Teams winner;

        public EndGameState(Game1 game, Teams winner) : base(
            winner == Teams.Alpha ?
                new string[] { "Continue", "Retry" } :
                new string[] { "Retry", "Quit"}
        )
        {
            this.winner = winner;
            gameRef = game;
        }

        public override void Draw(InLevelState superState, GameTime gameTime)
        {
            base.Draw(superState, gameTime);
        }

        public override void Update(InLevelState superState, GameTime gameTime)
        {
            if (InputManager.IsDiscreteDown(Keys.Down))
                options.MoveToNext();
            else if (InputManager.IsDiscreteDown(Keys.Up))
                options.MoveToPrevious();
            else if (InputManager.IsDiscreteDown(Keys.Enter) || InputManager.IsDiscreteDown(Keys.Z))
            {
                HandleMenuSelection(superState);
            }
        }

        protected override void HandleMenuSelection(InLevelState superState)
        {

            if (options.SelectedIndex == 0 && winner == Teams.Alpha)
            {
                gameRef.State = new WorldMapState();
            }
            else if (options.SelectedIndex == 0 || (options.SelectedIndex == 1 && winner == Teams.Alpha)) {
                gameRef.State = new InLevelState(gameRef, superState.scenarioName);
            }
            else
                gameRef.State = new WorldMapState();

        }
    }
}
