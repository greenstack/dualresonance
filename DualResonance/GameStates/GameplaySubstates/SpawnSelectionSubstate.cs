﻿using System;
using System.Collections.Generic;
using DualResonance.Deployables.Cards;
using DualResonance.Deployables.Units;
using DualResonance.Graphics;
using Microsoft.Xna.Framework;

namespace DualResonance.GameStates.GameplaySubstates
{
    /// <summary>
    /// For managing spawn selection.
    /// </summary>
    class SpawnSelectionSubstate : MenuSubState<int>
    {
        DeckInfo playerDeck;

        private static int[] buildOptions(GameSession session)
        {
            DeckInfo playerDeck = session.GetPlayer(0).CurrentDeck;
            Teams playerTeam = session.GetPlayer(0).Team;
            List<int> availableMercs = new List<int>();
            int uid;
            for (int i = 0; i < playerDeck.UnitsInDeck; ++i)
            {
                uid = playerDeck.GetMercenary(i).Id;
                if (!session.UnitInPlay(playerTeam, uid))
                    availableMercs.Add(i);
            }
            availableMercs.Add(-1);
            return availableMercs.ToArray();
        }

        public override void Draw(InLevelState superState, GameTime gameTime)
        {
            base.Draw(superState, gameTime);
            if (options.SelectedItem == -1) return;
            // Draw the selected merc's data on the edge of the screen
            MercenaryCard selectedCard = playerDeck.GetMercenary(options.SelectedItem);
            ActiveMercenary merc = new ActiveMercenary(selectedCard, Teams.Alpha);
            AssetManager.Instance.DrawUnitQuery(merc, new Vector2(0, InLevelState.QueryLocation), true);
        }

        public SpawnSelectionSubstate(GameSession session) : base(buildOptions(session))
        {
            playerDeck = session.GetPlayer(0).CurrentDeck;
            Teams playerTeam = session.GetPlayer(0).Team;
            string uname;
            List<string> names = new List<string>();
            int uid;
            for (int i = 0; i < playerDeck.UnitsInDeck; ++i)
            {
                uname = playerDeck.GetMercenary(i).Name;
                uid = playerDeck.GetMercenary(i).Id;
                if (!session.UnitInPlay(playerTeam, uid))
                    names.Add(uname);
            }
            names.Add("Back");
            
            Overwrite = names.ToArray();
        }

        protected override void HandleMenuSelection(InLevelState superState)
        {
            if (options.SelectedItem == -1)
            {
                superState.subState = new FreeCursorSubstate();
                return;
            }

            try
            {
                superState.level.Spawn(superState.player, superState.cursor, options.SelectedItem);
                superState.subState = new FreeCursorSubstate();
            }
            catch (InvalidDRActionException e)
            {
                superState.RegisterTemporaryMessage(e.Message);
            }
        }
    }
}
