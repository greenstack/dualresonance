﻿using System;
using System.Collections.Generic;

namespace DualResonance.GameStates.GameplaySubstates
{
    class EmptySpaceSelectedSubstate : MenuSubState<int>
    {
        private const int SPAWN = 0;
        private const int END_TURN = 1;
        private const int BACK = 2;

        private static int[] GenerateMenu(InLevelState superState)
        {
            List<int> items = new List<int>();
            if (superState.level.IsInTeamBounds(Teams.Alpha, superState.cursor))
                items.Add(SPAWN);
            items.Add(END_TURN);
            items.Add(BACK);
            return items.ToArray();
        }

        public EmptySpaceSelectedSubstate(InLevelState superState) : base(GenerateMenu(superState))
        {
            List<string> overrides = new List<string>();
            if (options.ItemCount == 3)
                overrides.Add("Spawn");
            overrides.Add("End Turn");
            overrides.Add("Back");
            Overwrite = overrides.ToArray();
        }

        protected override void HandleMenuSelection(InLevelState superState)
        {
            switch (options.SelectedItem)
            {
                case SPAWN:
                    // Spawn
                    superState.subState = new SpawnSelectionSubstate(superState.level);
                    break;
                case END_TURN:
                    // Commander Ability
                    //throw new NotImplementedException();
                //case 2:
                    // End Turn
                    superState.level.EndTurn();
                    goto case 2;
                case BACK:
                    // Back
                    superState.subState = new FreeCursorSubstate();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("Invalid selected index.");
            }
        }
    }
}
