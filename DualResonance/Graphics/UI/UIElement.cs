﻿using Microsoft.Xna.Framework;

namespace DualResonance.Graphics.UI
{
    /// <summary>
    /// Represents an element that generally has some level of interaction.
    /// </summary>
    public abstract class UIElement
    {
        /// <summary>
        /// If the element should be drawn to the screen or not.
        /// </summary>
        protected bool Visible = true;
        /// <summary>
        /// If the elemeny should be updated or not.
        /// </summary>
        protected bool Enabled = true;

        protected Vector2 position;
        protected Vector2 scale;

        public Color Color = Color.Black;

        public UIElement(Vector2 position, Vector2 scale)
        {
            this.position = position;
            this.scale = scale;
        }

        /// <summary>
        /// Draws the element to the screen.
        /// </summary>
        /// <param name="gameTime">Time elapsed since the last draw.</param>
        public abstract void Draw(GameTime gameTime);

        /// <summary>
        /// Updates the element.
        /// </summary>
        /// <param name="gameTime">Time elapsed since the last update.</param>
        public abstract void Update(GameTime gameTime);
    }
}
