﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DualResonance.Graphics
{
    class BillboardRender : IRender
    {
        Model model;
        public Vector3 Position;

        public Vector3 Rotation;

        public Matrix PerspectiveMatrix { get; set; }
        public Matrix ViewMatrix { get; set; }

        public BillboardRender(Model model)
        {
            AssetManager am = AssetManager.Instance;
            if (am.PerspectiveMatrix == null || am.ViewMatrix == null)
                throw new NullReferenceException("Cannot create BillboardRender before AssetManager initialization.");

            ViewMatrix = am.ViewMatrix;
            PerspectiveMatrix = am.PerspectiveMatrix;
            this.model = model;
        }

        internal void Draw(Game game, Camera3D camera, Texture2D texture, Vector3? color, Vector3? scale)
        {
            game.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            var cameraTransform = camera.Transform;
            Matrix transform = Matrix.CreateRotationX(Rotation.X) * Matrix.CreateRotationY(Rotation.Y)
                * Matrix.CreateScale(scale != null ? (Vector3)scale : Vector3.One) * Matrix.CreateTranslation(Position);

            var mesh = model.Meshes[0];
            foreach(BasicEffect effect in mesh.Effects)
            {
                effect.World = transform * cameraTransform;
                effect.View = ViewMatrix;
                effect.Projection = PerspectiveMatrix;
                effect.LightingEnabled = false;
                effect.TextureEnabled = true;
                effect.Texture = texture;   
            }
            mesh.Draw();
        }
    }
}
