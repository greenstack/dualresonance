﻿namespace DualResonance
{
    class Menu<T>
    {
        private const int NotInList = -1;

        private int selectedIndex = 0;
        /// <summary>
        /// The currently selected index of the menu. If comparing this value
        /// to another, use the IsSelected methods instead.
        /// </summary>
        public int SelectedIndex {
            get => selectedIndex;
            set => selectedIndex = (value + ItemCount) % ItemCount;
        }

        public T SelectedItem => MenuItems[selectedIndex];

        /// <summary>
        /// Retrieves the number of elements in this menu.
        /// </summary>
        public int ItemCount { get => MenuItems.Length; }

        readonly private T[] MenuItems;

        public Menu(T[] menuItems)
        {
            MenuItems = menuItems;
        }

        /// <summary>
        /// Advances the currently selected index pointer.
        /// </summary>
        public void MoveToNext()
        {
            ++SelectedIndex;
        }

        /// <summary>
        /// Pulls the currently selected index pointer back.
        /// </summary>
        public void MoveToPrevious()
        {
            --SelectedIndex;
        }

        /// <summary>
        /// Determines if the desired index is the selected one.
        /// </summary>
        /// <param name="index">The index to check against.</param>
        /// <returns>True if the index is the selected one.</returns>
        public bool IsSelected(int index)
        {
            return SelectedIndex == index;
        }

        /// <summary>
        /// Determines if the provided item as at the selected index.
        /// </summary>
        /// <param name="item">The item to check.</param>
        /// <returns></returns>
        public bool IsSelected(T item)
        {
            int index = GetIndexOfItem(item);
            if (index == NotInList)
                return false;
            return IsSelected(index);
        }

        /// <summary>
        /// Retrieves the item at the given menu index.
        /// </summary>
        /// <param name="index">The index to be checked.</param>
        /// <returns></returns>
        public T GetItem(int index)
        {
            return MenuItems[index];
        }
        /// <summary>
        /// Finds the location of the given item.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <returns>The index of the item, or -1 if not found.</returns>
        public int GetIndexOfItem(T item)
        {
            for (int i = 0; i < ItemCount; ++i)
            {
                if (MenuItems[i].Equals(item))
                    return i;
            }
            return NotInList;
        }
    }
}
