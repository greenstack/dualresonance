﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DualResonance
{
    internal static class InputManager
    {
        /// <summary>
        /// The amount of time to wait before registering another discrete input option.
        /// </summary>
        public const float DiscreteInputDelay = 200;
        public static float UntilNextInput { get; private set; }

        /// <summary>
        /// Determines if the key is down and the delay between inputs has passed.
        /// </summary>
        /// <param name="key">The key to check if down.</param>
        /// <returns>True if the input should be registered.</returns>
        public static bool IsDiscreteDown(Keys key)
        {
            if (Keyboard.GetState().IsKeyDown(key) &&
                UntilNextInput == 0)
            {
                UntilNextInput = DiscreteInputDelay;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determines if the given key is down.
        /// </summary>
        /// <param name="key">The key to check if down.</param>
        /// <returns>True if the key is down.</returns>
        public static bool IsContinuousKeyDown(Keys key)
        {
            return Keyboard.GetState().IsKeyDown(key);
        }

        public static void Update(GameTime gameTime)
        {
            if (UntilNextInput == 0) return;
            UntilNextInput = MathHelper.Max(UntilNextInput - gameTime.ElapsedGameTime.Milliseconds, 0);
        }

    }
}
