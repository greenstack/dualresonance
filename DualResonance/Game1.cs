﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using DualResonance.Graphics;
using DualResonance.GameStates;

namespace DualResonance
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        protected Vector3 modelPosition = Vector3.Zero;

        FrameCounter frameCounter = new FrameCounter();

        public float WindowScaleMultiplier { get; private set; } = 1;

        internal AbstractGameState State;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
#if !DEMO
            State = new MainMenuState();
#endif
            Deployables.Cards.UnitImporter.LoadMercenaries("Deployables/Cards/dra_base.json");
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            float aspectRatio = GraphicsDevice.Viewport.Width /
                GraphicsDevice.Viewport.Height;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            AssetManager.Initialize(this, spriteBatch);
            AssetManager.Instance.PerspectiveMatrix = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.ToRadians(45.0f),
                aspectRatio,
                1.0f,
                10000.0f);
#if DEMO
            State = new InLevelState(this, "copper10.drl");
#endif
            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            //spriteBatch = new SpriteBatch(GraphicsDevice);
            AssetManager.Instance.StatIcons = Content.Load<Texture2D>("Resonance");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        private void Resize()
        {
            WindowScaleMultiplier *= 2;
            graphics.PreferredBackBufferWidth *= 2;
            graphics.PreferredBackBufferHeight *= 2;
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            InputManager.Update(gameTime);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Delete))
                Exit();
            if (InputManager.IsDiscreteDown(Keys.OemTilde))
                Resize();
            State.Update(this, gameTime);

            //Vector2 leftStick = GamePad.GetState(PlayerIndex.One).ThumbSticks.Right;
            // TODO: Add your update logic here

            Window.Title = $"Dual Resonance ({System.Math.Round((double)frameCounter.CurrentFramesPerSecond)} FPS)";
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // We should probably draw the 3D models before the UI,
            // but until proven otherwise...
            spriteBatch.Begin();
            State.Draw(this, gameTime);
            spriteBatch.End();

            frameCounter.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

            base.Draw(gameTime);
        }
    }
}
