﻿using DualResonance.Deployables.Cards;
using DualResonance.Deployables.Units;
using DualResonance.Graphics;
using DualResonance.Sessions;
using DualResonance.Utilities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace DualResonance
{
    class LevelBuilder
    {

        const int TITLE_STAGE = 0;
        const int DECKLIST_STAGE = 1;
        const int SPAWN_LIST_STAGE = 2;
        const int GRID_STAGE = 3;
        const int WIN_CONDITIONS = 4;
        const int LOSE_CONDITIONS = 5;
        const int SCENE_DATA_STAGE = 6;

        public static Tuple<GameSession, LightSource> FromLevel(Stream manifestStream)
        {
            GameSession built = new GameSession();
            string[] lines;
            using (StreamReader sr = new StreamReader(manifestStream))
                lines = sr.ReadToEnd().Split('\n');
            int currentStage = 0;
            List<ActiveMercenary> spawnList = new List<ActiveMercenary>();
            int row = 0;
            LightSource lightSource = new LightSource();
            foreach (var line in lines)
            {
                var _line = Regex.Replace(line, @"\n|\r", "");
                // Ignore commented lines
                if (_line == string.Empty) continue;
                if (_line[0] == '#') continue;
                if (_line == "Decklist" ||
                    _line == "Spawn-List" ||
                    _line == "Grid" ||
                    _line == "Win-Conditions" ||
                    _line == "Lose-Conditions" ||
                    _line == "Scene-Data")
                {
                    currentStage++;
                    continue;
                }
                switch (currentStage)
                {
                    case TITLE_STAGE:
                        built.Name = _line;
                        continue;
                    case DECKLIST_STAGE:
                        InterpretDecklistLine(built, _line);
                        continue;
                    case SPAWN_LIST_STAGE:
                        spawnList.Add(BuildMerc(_line));
                        continue;
                    case GRID_STAGE:
                        SpawnOnRow(built, spawnList, _line, row++);
                        continue;
                    case WIN_CONDITIONS:
                        RegisterCondition(built, _line, Teams.Alpha);
                        continue;
                    case LOSE_CONDITIONS:
                        RegisterCondition(built, _line, Teams.Beta);
                        continue;
                    case SCENE_DATA_STAGE:
                        var args = _line.Split(' ');
                        Vector3 data = new Vector3(
                                        float.Parse(args[1]),
                                        float.Parse(args[2]),
                                        float.Parse(args[3]));
                        switch (args[0])
                        {
                            case "Ambient":
                                lightSource.AmbientColor = data;
                                continue;
                            case "KeyLightC":
                                lightSource.KeyLightColor = data;
                                continue;
                            case "KeyLightD":
                                lightSource.KeyDirection = data;
                                continue;
                            case "FillLightC":
                                lightSource.FillLightColor = data;
                                continue;
                            case "FillLightD":
                                lightSource.FillDirection = data;
                                continue;
                            default:
                                break;
                        }
                        continue;
                    default:
                        break;
                }
            }

            return new Tuple<GameSession, LightSource>(built, lightSource);
        }

        private static void InterpretDecklistLine(GameSession session, string line)
        {
            DeckInfo deck = new DeckInfo();
            string[] parts = line.Split(' ');
            Player player;
            if (parts[0] == "P")
                player = new Player();
            else if (parts[0] == "A")
                player = new AIPlayer();
            else
                throw new InvalidDataException("The level data is malformed - invalid player type parameter.");
            player.CurrentDeck = deck;

            if (parts[1] == "A")
                player.Team = Teams.Alpha;
            else if (parts[1] == "B")
                player.Team = Teams.Beta;
            else
                throw new InvalidDataException("The level data is malformed - invalid team provided.");

            int commanderId = int.Parse(parts[2]);
            if (commanderId != -1)
                deck.Commander = Commanders.GetCard(commanderId);
            else
                deck.Commander = null;

            int cardsToPull = int.Parse(parts[3]);
            for (int i = 4; i < 4 + cardsToPull; i++)
                deck.AddMercenary(int.Parse(parts[i]));

            session.AddPlayer(player);
        }

        private static ActiveMercenary BuildMerc(string line)
        {
            string[] parts = line.Split(' ');
            int mercId = int.Parse(parts[0]);
            int mercOwner = int.Parse(parts[2]);
            Teams team;
            if (parts[1] == "A")
                team = Teams.Alpha;
            else if (parts[1] == "B")
                team = Teams.Beta;
            else
                throw new InvalidDataException("Invalid level data - spawn-list had invalid team.");
            ActiveMercenary merc = new ActiveMercenary(MercenaryCards.GetMercenary(mercId), team)
            {
                OwnerPlayerId = mercOwner,
            };
            return merc;
        }

        private static void SpawnOnRow(GameSession session, List<ActiveMercenary> mercs, string line, int row)
        {
            string[] data = line.Split(' ');
            for (int i = 0; i < 5; i++)
            {
                int spawned_Id = int.Parse(data[i]);
                if (spawned_Id > 0)
                {
                    spawned_Id--;
                    ActiveMercenary merc
                        = new ActiveMercenary(
                            MercenaryCards.GetMercenary(mercs[spawned_Id].MercenaryId),
                            mercs[spawned_Id].Team)
                        {
                            OwnerPlayerId = mercs[spawned_Id].OwnerPlayerId,
                        };
                    session.Spawn(merc, new BoardCoordinate(i, row));
                }
            }
        }

        private static void RegisterCondition(GameSession session, string line, Teams winner)
        {
            var parts = line.Split(' ');
            Condition condition;
            switch (parts[0])
            {
                case "Units":
                    int unitCount = int.Parse(parts[3]);
                    Teams team;
                    if (parts[1] == "A")
                        team = Teams.Alpha;
                    else if (parts[1] == "B")
                        team = Teams.Beta;
                    else throw new InvalidDataException("Invalid team given in condition list.");
                    condition = new UnitCountCondition(unitCount, team, winner, Comparisons.CFromString(parts[2]), false);
                    break;
                case "Unit":
                    unitCount = int.Parse(parts[2]);
                    Comparer comparer = Comparisons.CFromString(parts[3]);
                    int unitId = int.Parse(parts[4]);
                    if (parts[1] == "A")
                        team = Teams.Alpha;
                    else if (parts[1] == "B")
                        team = Teams.Beta;
                    else throw new InvalidDataException("Invalid team given in condition list.");
                    condition = new UnitExistsCondition(team, unitId, winner, true);
                    break;
                case "Commander-HP":
                    int commanderId = int.Parse(parts[1]);
                    comparer = Comparisons.CFromString(parts[2]);
                    int limit = int.Parse(parts[3]);
                    condition = new CommanderHealthCondition(commanderId, limit, winner, comparer, false);
                    break;
                default:
                    throw new InvalidDataException("Unrecognized win condition.");
            }
            session.AddWinCondtion(condition);
        }
    }
}
